package main

import (
	"bitbucket.org/timorl/pekawu/pkw"
	"html/template"
	"net/http"
	"strconv"
)

var templates = template.Must(template.ParseGlob("templates/*.html"))

func obywateleSearchResultsForm(w http.ResponseWriter, searchResults []pkw.Obywatel) {
	tmp := struct {
		ObywateleFound []pkw.Obywatel
		Komisje        []pkw.KomisjaObwodowa
		Wybory         []pkw.Wybory
	}{
		searchResults,
		pkw.GetKomisjeObwodowe(),
		pkw.GetWybory(),
	}
	templates.ExecuteTemplate(w, "obywateleSearchResults.html", tmp)
}

func obywateleSearchHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("search") == "Szukaj" {
		obywateleSearchResultsForm(w, pkw.SearchObywatele(r.Form))
	} else {
		templates.ExecuteTemplate(w, "obywateleSearchForm.html", nil)
	}
}

func obywateleAddHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("add") == "Dodaj" {
		ob, err := pkw.ObywatelFromForm(r.Form)
		if err != nil {
			templates.ExecuteTemplate(w, "obywateleAddForm.html", nil)
		} else {
			result := pkw.AddObywatel(ob)
			templates.ExecuteTemplate(w, "obywateleAddResults.html", result)
		}
	} else {
		templates.ExecuteTemplate(w, "obywateleAddForm.html", nil)
	}
}

func addWyboryForm(w http.ResponseWriter) {
	types := pkw.GetTypyWyborow()
	templates.ExecuteTemplate(w, "wyboryAddForm.html", types)
}

func wyboryAddHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("add") == "Dodaj" {
		wb, err := pkw.WyboryFromForm(r.Form)
		if err != nil {
			addWyboryForm(w)
		} else {
			result := pkw.AddWybory(wb)
			templates.ExecuteTemplate(w, "wyboryAddResults.html", result)
		}
	} else {
		addWyboryForm(w)
	}
}

func addNadkomisjeForm(w http.ResponseWriter) {
	types := pkw.GetNadkomisje()
	templates.ExecuteTemplate(w, "nadkomisjeAddForm.html", types)
}

func nadkomisjeAddHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("add") == "Dodaj" {
		nk, err := pkw.NadkomisjaFromForm(r.Form)
		if err != nil {
			addNadkomisjeForm(w)
		} else {
			result := pkw.AddNadkomisja(nk)
			templates.ExecuteTemplate(w, "nadkomisjeAddResults.html", result)
		}
	} else {
		addNadkomisjeForm(w)
	}
}

func nadkomisjeCzlonkowieHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("add_czlonek") == "Dodaj członka" {
		templates.ExecuteTemplate(w, "nadkomisjeCzlonkowieForm.html", r.FormValue("id"))
	} else if r.FormValue("add_czlonek") == "Dodaj" {
		nk, err := pkw.CzlonekNadkomisjiFromForm(r.Form)
		if err != nil {
			templates.ExecuteTemplate(w, "nadkomisjeCzlonkowieResults.html", "Porażka")
		} else {
			result := pkw.AddCzlonekNadkomisji(nk)
			templates.ExecuteTemplate(w, "nadkomisjeCzlonkowieResults.html", result)
		}
	} else {
		searchResults := pkw.GetNadkomisje()
		templates.ExecuteTemplate(w, "nadkomisjeCzlonkowieShow.html", searchResults)
	}
}

func addMandatyForm(w http.ResponseWriter) {
	addMnd := struct {
		Nadkomisje []pkw.Nadkomisja
		Wybory     []pkw.Wybory
	}{
		pkw.GetNadkomisje(),
		pkw.GetWybory(),
	}
	templates.ExecuteTemplate(w, "mandatyAddForm.html", addMnd)
}

func mandatyAddHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("add") == "Dodaj" {
		nk, err := pkw.MandatyFromForm(r.Form)
		if err != nil {
			addMandatyForm(w)
		} else {
			result := pkw.AddMandaty(nk)
			templates.ExecuteTemplate(w, "mandatyAddResults.html", result)
		}
	} else {
		addMandatyForm(w)
	}
}

func addKomisjeForm(w http.ResponseWriter) {
	types := pkw.GetNadkomisje()
	templates.ExecuteTemplate(w, "komisjeAddForm.html", types)
}

func komisjeAddHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("add") == "Dodaj" {
		nk, err := pkw.KomisjaObwodowaFromForm(r.Form)
		if err != nil {
			addKomisjeForm(w)
		} else {
			idKomisji, err := pkw.AddKomisjaObwodowa(nk)
			if err != nil {
				templates.ExecuteTemplate(w, "komisjeAddResults.html", err)
			} else {
				result := pkw.AddCzlonkowieKomisjiObwodowychFromForm(r.Form, idKomisji)
				templates.ExecuteTemplate(w, "komisjeAddResults.html", "Stworzono komisję zawierającą "+strconv.Itoa(result)+" członków.")
			}
		}
	} else {
		addKomisjeForm(w)
	}
}

func komitetAddHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("add") == "Dodaj" {
		nk, err := pkw.KomitetFromForm(r.Form)
		if err != nil {
			templates.ExecuteTemplate(w, "komitetyAddForm.html", nil)
		} else {
			result := pkw.AddKomitety(nk)
			templates.ExecuteTemplate(w, "komitetyAddResults.html", result)
		}
	} else {
		templates.ExecuteTemplate(w, "komitetyAddForm.html", nil)
	}
}

func addListForm(w http.ResponseWriter) {
	komitetNadkomisja := struct {
		Komitety []pkw.Komitet
		Wybory   []pkw.Wybory
	}{
		pkw.GetKomitety(),
		pkw.GetWybory(),
	}
	templates.ExecuteTemplate(w, "listaAddForm.html", komitetNadkomisja)
}

func listaAddHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("add") == "Kontynuuj" {
		idWyb, err := strconv.ParseInt(r.FormValue("id_wyborow"), 0, 0)
		idKom, err1 := strconv.ParseInt(r.FormValue("id_komitetu"), 0, 0)
		if err != nil || err1 != nil {
			addListForm(w)
		} else {
			frm := struct {
				Mandaty    []pkw.Mandaty
				IdWyborow  int
				IdKomitetu int
			}{
				pkw.GetMandatyOfGivenWybory(int(idWyb)),
				int(idWyb),
				int(idKom),
			}
			templates.ExecuteTemplate(w, "listaAddMandatForm.html", frm)
		}
	} else if r.FormValue("add") == "Dodaj" {
		nk, err := pkw.ListaFromForm(r.Form)
		if err != nil {
			addListForm(w)
		} else {
			result, err := pkw.AddListy(nk)
			if err != nil {
				panic(err)
				templates.ExecuteTemplate(w, "listaAddResults.html", "Nie udało się stworzyć listy.")
			} else {
				templates.ExecuteTemplate(w, "listaAddCzlonkowieForm.html", result)
			}
		}
	} else if r.FormValue("add_kand") == "Dodaj" {
		var result int
		kndc, err := pkw.KandydaciFromForm(r.Form)
		if err != nil {
			panic(err)
		}
		for _, k := range kndc {
			if pkw.AddKandydata(k) {
				result++
			}
		}
		templates.ExecuteTemplate(w, "listaAddResults.html", "Dodano listę z "+strconv.Itoa(result)+" kandydatami.")
	} else {
		addListForm(w)
	}
}

func addPodpisyForm(w http.ResponseWriter) {
	templates.ExecuteTemplate(w, "podpisyAddForm.html", pkw.GetListy())
}

func podpisyAddHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("add") == "Dodaj" {
		var result int
		pdps, err := pkw.PodpisyFromForm(r.Form)
		if err != nil {
			panic(err)
		}
		for _, p := range pdps {
			if pkw.AddPodpisy(p) {
				result++
			}
		}
		templates.ExecuteTemplate(w, "podpisyAddResults.html", "Dodano "+strconv.Itoa(result)+" podpisów.")
	} else {
		addPodpisyForm(w)
	}
}

func showWynikiForm(w http.ResponseWriter) {
	templates.ExecuteTemplate(w, "showWynikiForm.html", pkw.GetPastWybory())
}

func wynikiShowHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("add") == "Wybierz" {
		idWyb, err := strconv.ParseInt(r.FormValue("id_wyborow"), 0, 0)
		if err != nil {
			showWynikiForm(w)
			return
		}
		mandaty := pkw.GetMandatyOfGivenWybory(int(idWyb))
		type mandatWyniki struct {
			Mandat   pkw.Mandaty
			Wyniki   []pkw.Wynik
			Zdobywcy []pkw.Obywatel
		}
		var mdtWn []mandatWyniki
		for _, m := range mandaty {
			mdtWn = append(mdtWn, mandatWyniki{m, pkw.GetResultsOfGivenMandat(m.IdWyborow, m.IdNadkomisji), pkw.WhoGot(m.IdWyborow, m.IdNadkomisji)})
		}
		tmp := struct {
			Frekwencja   float32
			MandatWyniki []mandatWyniki
		}{
			pkw.GetFrekwencjaInWybory(int(idWyb)) * 100,
			mdtWn,
		}
		templates.ExecuteTemplate(w, "showWynikiResults.html", tmp)
	} else {
		showWynikiForm(w)
	}
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	templates.ExecuteTemplate(w, "index.html", nil)
}

func obywateleObwodyHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("add") == "Przypisz" {
		toAdd, err := pkw.ObywateleObwodyFromForm(r.Form)
		if err != nil {
			panic(err)
		}
		var addedNum int
		for _, oo := range toAdd {
			if pkw.AddObywateleObwody(oo) {
				addedNum++
			}
		}
		templates.ExecuteTemplate(w, "obywateleObwodyResults.html", addedNum)
	}
}

//Wybory stuff from here
func wyboryStartHandler(w http.ResponseWriter, r *http.Request) {
	templates.ExecuteTemplate(w, "wyboryStart.html", nil)
}

func wyborySelectHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("start") == "Rozpocznij" {
		wybSel := struct {
			Pesel  string
			Wybory []pkw.Wybory
		}{
			r.FormValue("pesel"),
			pkw.GetCurrentWyboryOfAGivenObywatel(r.FormValue("pesel")),
		}
		templates.ExecuteTemplate(w, "wyborySelect.html", wybSel)
	} else {
		wyboryStartHandler(w, r)
	}
}

func listaSelectHandler(w http.ResponseWriter, r *http.Request) {
	idWyb, err := strconv.ParseInt(r.FormValue("wybory"), 0, 0)
	if err == nil {
		lisSel := struct {
			Pesel         string
			IdWyborow     int
			ListyKomitety []pkw.ListaZKomitetem
		}{
			r.FormValue("pesel"),
			int(idWyb),
			pkw.GetKomitetOfLista(pkw.GetListyOfGivenWyboryAndObywatel(r.FormValue("pesel"), int(idWyb))),
		}
		templates.ExecuteTemplate(w, "listaSelect.html", lisSel)
	} else {
		wyboryStartHandler(w, r)
	}
}

func kandydatSelectHandler(w http.ResponseWriter, r *http.Request) {
	idWyb, err := strconv.ParseInt(r.FormValue("id_wyborow"), 0, 0)
	idLis, err1 := strconv.ParseInt(r.FormValue("lista"), 0, 0)
	if err == nil && err1 == nil {
		if idLis == -1 {
			idKom := pkw.GetWhereDoesObywatelVote(r.FormValue("pesel"), int(idWyb))
			idNdk := pkw.GetNadkomisjaOfGivenWyboryAndObwod(int(idWyb), idKom)
			if pkw.VoteIllegal(idKom, int(idWyb), idNdk, r.FormValue("pesel")) {
				templates.ExecuteTemplate(w, "glosResult.html", "Nieważny głos oddany.")
				return
			} else {
				wyboryStartHandler(w, r)
				return
			}
		}
		kand := pkw.GetKandydaciFromLista(int(idLis))
		type kandydatNazwa struct {
			Id       int
			Imie     string
			Nazwisko string
		}
		var knd []kandydatNazwa
		for _, k := range kand {
			ob := pkw.SearchObywatele(map[string][]string{"peselLubNrDowodu": {k.Pesel}})
			knd = append(knd, kandydatNazwa{k.Id, ob[0].Imie, ob[0].Nazwisko})
		}
		kanSel := struct {
			Pesel     string
			IdWyborow int
			IdListy   int
			Kandydaci []kandydatNazwa
		}{
			r.FormValue("pesel"),
			int(idWyb),
			int(idLis),
			knd,
		}
		templates.ExecuteTemplate(w, "kandydatSelect.html", kanSel)
	} else {
		wyboryStartHandler(w, r)
	}
}

func voteHandler(w http.ResponseWriter, r *http.Request) {
	idWyb, err := strconv.ParseInt(r.FormValue("id_wyborow"), 0, 0)
	idKan, err1 := strconv.ParseInt(r.FormValue("kandydat"), 0, 0)
	if err == nil && err1 == nil {
		idKom := pkw.GetWhereDoesObywatelVote(r.FormValue("pesel"), int(idWyb))
		if pkw.Vote(idKom, int(idKan), r.FormValue("pesel")) {
			templates.ExecuteTemplate(w, "glosResult.html", "Głos oddany.")
		} else {
			wyboryStartHandler(w, r)
		}
	} else {
		wyboryStartHandler(w, r)
	}
}

func startVotingServer() {
	vServer := http.NewServeMux()
	vServer.HandleFunc("/style.css", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "media/style.css")
	})
	vServer.HandleFunc("/", wyboryStartHandler)
	vServer.HandleFunc("/wyborySelect", wyborySelectHandler)
	vServer.HandleFunc("/listaSelect", listaSelectHandler)
	vServer.HandleFunc("/kandydatSelect", kandydatSelectHandler)
	vServer.HandleFunc("/vote", voteHandler)
	http.ListenAndServe(":8181", vServer)
}

func main() {
	go startVotingServer()
	http.HandleFunc("/style.css", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "media/style.css")
	})
	http.HandleFunc("/", handleIndex)
	http.HandleFunc("/obywateleSearch", obywateleSearchHandler)
	http.HandleFunc("/obywateleAdd", obywateleAddHandler)
	http.HandleFunc("/wyboryAdd", wyboryAddHandler)
	http.HandleFunc("/nadkomisjeAdd", nadkomisjeAddHandler)
	http.HandleFunc("/nadkomisjeCzlonkowie", nadkomisjeCzlonkowieHandler)
	http.HandleFunc("/mandatyAdd", mandatyAddHandler)
	http.HandleFunc("/komisjeAdd", komisjeAddHandler)
	http.HandleFunc("/obywateleObwody", obywateleObwodyHandler)
	http.HandleFunc("/komitetAdd", komitetAddHandler)
	http.HandleFunc("/listaAdd", listaAddHandler)
	http.HandleFunc("/podpisyAdd", podpisyAddHandler)
	http.HandleFunc("/wynikiShow", wynikiShowHandler)
	http.ListenAndServe(":8080", nil)
}
