--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: funkcja_w_komisji_obwodowej; Type: TYPE; Schema: public; Owner: pkw
--

CREATE TYPE funkcja_w_komisji_obwodowej AS ENUM (
    'przewodniczacy',
    'zastepca',
    'sekretarz',
    'czlonek'
);


ALTER TYPE public.funkcja_w_komisji_obwodowej OWNER TO pkw;

--
-- Name: ordynacja; Type: TYPE; Schema: public; Owner: pkw
--

CREATE TYPE ordynacja AS ENUM (
    'Dhondt',
    'wiekszosciowe',
    'jednoosobowe'
);


ALTER TYPE public.ordynacja OWNER TO pkw;

--
-- Name: edycja_typu_wyborow(); Type: FUNCTION; Schema: public; Owner: pkw
--

CREATE FUNCTION edycja_typu_wyborow() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF  EXISTS (SELECT * FROM wybory WHERE typ = NEW.nazwa)
	THEN
		RAISE EXCEPTION 'Zmiana zasad wyborow po ich ogloszeniu jest zakazana';
	END IF;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.edycja_typu_wyborow() OWNER TO pkw;

--
-- Name: jedna_lista(); Type: FUNCTION; Schema: public; Owner: pkw
--

CREATE FUNCTION jedna_lista() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF  EXISTS (SELECT * FROM wybory w
		JOIN mandaty m ON (w.id=m.id_wyborow)
		JOIN listy l ON (l.id_nadkomisji = m.id_nadkomisji and l.id_wyborow = m.id_wyborow)
		JOIN kandydaci k ON (k.id_listy = l.id)
		JOIN obywatele o ON (o.pesel = k.pesel)
		WHERE  w.id = (SELECT MAX(ww.id) FROM wybory ww
			JOIN mandaty mm ON (ww.id=mm.id_wyborow)
			JOIN nadkomisje nkk ON (mm.id_nadkomisji = nkk.id)
			JOIN listy ll ON (ll.id_nadkomisji = nkk.id)
			WHERE ll.id = NEW.id_listy)
		AND o.pesel = NEW.pesel)
	THEN
		RAISE EXCEPTION 'Kandydat jest już zgłoszony w tych wyborach na jednej z list';
	END IF;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.jedna_lista() OWNER TO pkw;

--
-- Name: prawa_kandydatow(); Type: FUNCTION; Schema: public; Owner: pkw
--

CREATE FUNCTION prawa_kandydatow() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF EXISTS (SELECT * FROM obywatele WHERE pesel=NEW.pesel and pozbawiony_praw)
	THEN
		RAISE EXCEPTION 'Zgłaszany kandydat nie posiada praw wyborczych zasiada obywatel pozbawiony praw';
	END IF;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.prawa_kandydatow() OWNER TO pkw;

--
-- Name: prawa_wyborcow(); Type: FUNCTION; Schema: public; Owner: pkw
--

CREATE FUNCTION prawa_wyborcow() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF EXISTS (SELECT * FROM obywatele o 
		   JOIN obywatele_obwody oo ON o.pesel = oo.pesel 
		   JOIN wybory w ON w.id = oo.id_wyborow
		   WHERE o.pesel=NEW.pesel AND NEW.id_wyborow = w.id AND (o.pozbawiony_praw OR (w.koniec - o.data_urodzenia < '18 years')))
	THEN
		RAISE EXCEPTION 'Zgłaszany wyborca nie posiada praw wyborczych';
	END IF;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.prawa_wyborcow() OWNER TO pkw;

--
-- Name: sprawdz_czlonkow_komisji(); Type: FUNCTION; Schema: public; Owner: pkw
--

CREATE FUNCTION sprawdz_czlonkow_komisji() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	--POSIADANIE PRAW  OBYWATELSKICH
	IF EXISTS (SELECT * FROM obywatele WHERE pesel=NEW.pesel and pozbawiony_praw)
	THEN
		RAISE EXCEPTION 'W komisji zasiada obywatel pozbawiony praw';
	END IF;
	--ZAKAZ ŁĄCZENIA FUNKCJI
	IF EXISTS (SELECT * FROM czlonkowie_komisji_obwodowych WHERE pesel=NEW.pesel and id_komisji=NEW.id_komisji)
	THEN
		RAISE EXCEPTION 'Nie można łączyć dwóch funkcji';
	END IF;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.sprawdz_czlonkow_komisji() OWNER TO pkw;

--
-- Name: sprawdz_czlonkow_nadkomisji(); Type: FUNCTION; Schema: public; Owner: pkw
--

CREATE FUNCTION sprawdz_czlonkow_nadkomisji() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF    EXISTS (SELECT * FROM obywatele WHERE pesel=NEW.pesel and pozbawiony_praw)
	THEN
		RAISE EXCEPTION 'W nadkomisji zasiada obywatel pozbawiony praw';
	END IF;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.sprawdz_czlonkow_nadkomisji() OWNER TO pkw;

--
-- Name: sprawdz_pesel(); Type: FUNCTION; Schema: public; Owner: pkw
--

CREATE FUNCTION sprawdz_pesel() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare sum integer;
BEGIN
	sum = CAST(SUBSTRING(NEW.pesel, 1, 1) AS INTEGER) + 3* CAST(SUBSTRING(NEW.pesel, 2, 1) AS INTEGER) + 
	7*CAST(SUBSTRING(NEW.pesel, 3, 1) AS INTEGER) + 9* CAST(SUBSTRING(NEW.pesel, 4, 1) AS INTEGER) + 
	CAST(SUBSTRING(NEW.pesel, 5, 1) AS INTEGER) + 3* CAST(SUBSTRING(NEW.pesel, 6, 1) AS INTEGER) + 
	7*CAST(SUBSTRING(NEW.pesel, 7, 1) AS INTEGER) + 9* CAST(SUBSTRING(NEW.pesel, 8, 1) AS INTEGER) + 
	CAST(SUBSTRING(NEW.pesel, 9, 1) AS INTEGER) + 3* CAST(SUBSTRING(NEW.pesel, 10, 1) AS INTEGER) +
	CAST(SUBSTRING(NEW.pesel, 11, 1) AS INTEGER);

	IF (sum%10 != 0 OR left(NEW.pesel, 6) != to_char(NEW.data_urodzenia, 'YYMMDD'))
		THEN
		RAISE EXCEPTION 'Niepoprawny PESEL';
	END IF;
	RETURN NEW;
	EXCEPTION
		WHEN OTHERS THEN
			RAISE EXCEPTION 'Niepoprawny PESEL';

END;
$$;


ALTER FUNCTION public.sprawdz_pesel() OWNER TO pkw;

--
-- Name: sprawdz_prawa(); Type: FUNCTION; Schema: public; Owner: pkw
--

CREATE FUNCTION sprawdz_prawa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF NEW.pozbawiony_praw THEN
		--USUNIECIE KANDYDATA
		IF EXISTS (SELECT * FROM wybory w
			JOIN mandaty m ON (w.id=m.id_wyborow)
			JOIN listy l ON (l.id_nadkomisji = m.id_nadkomisji and l.id_wyborow = m.id_wyborow)
			JOIN kandydaci k ON (k.id_listy = l.id)
			JOIN obywatele o ON (o.pesel = k.pesel)
			WHERE w.poczatek>now() and o.pesel = NEW.pesel)
		THEN
			DELETE FROM kandydaci
			WHERE id = (SELECT k.id FROM wybory w
				JOIN mandaty m ON (w.id=m.id_wyborow)
				JOIN listy l ON (l.id_nadkomisji = m.id_nadkomisji and l.id_wyborow = m.id_wyborow)
				JOIN kandydaci k ON (k.id_listy = l.id)
				JOIN obywatele o ON (o.pesel = k.pesel)
				WHERE w.poczatek>now() and o.pesel = NEW.pesel);
			DELETE FROM obywatele_obwody
			WHERE pesel = NEW.pesel 
			AND id_wyborow IN (SELECT id FROM wybory WHERE poczatek>now());
		END IF; -- USUWAMY TYLKO KANDYDATOW W PRZYSZLYCH WYBORACH

		--SPRAWDZENIE, CZY OBYWATEL BEDZIE ZASIADAL W KOMISJACH
		IF EXISTS (SELECT * FROM wybory w
			JOIN mandaty m ON (w.id=m.id_wyborow)
			JOIN nadkomisje nk ON (m.id_nadkomisji = nk.id)
			JOIN czlonkowie_nadkomisji cn ON (nk.id = cn.id_nadkomisji)
			LEFT OUTER JOIN komisje_obwodowe_nadkomisje kon ON (nk.id = kon.id_nadkomisji)
			LEFT OUTER JOIN komisje_obwodowe ko ON (kon.id_komisji = ko.id)
			LEFT OUTER JOIN czlonkowie_komisji_obwodowych cko ON (cko.id_komisji = ko.id)
			WHERE w.poczatek>now() and (NEW.pesel = cn.pesel OR NEW.pesel = cko.pesel))
		THEN
			RAISE EXCEPTION 'Obywatel, ktoremu odebrano prawa ma zasiadac w jednej z komisji lub nadkomisji. Prosze wyznaczyc zastepstwo przed odebraniem praw ';
		END IF;

	END IF;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.sprawdz_prawa() OWNER TO pkw;

--
-- Name: termin_glosowania(); Type: FUNCTION; Schema: public; Owner: pkw
--

CREATE FUNCTION termin_glosowania() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF NOT EXISTS (SELECT * FROM wybory w
		JOIN mandaty m ON (w.id=m.id_wyborow)
		JOIN listy l ON (l.id_nadkomisji = m.id_nadkomisji and l.id_wyborow = m.id_wyborow)
		JOIN kandydaci k ON (k.id_listy = l.id)
		WHERE k.id=NEW.id_kand and now()<=w.koniec and now()>=w.poczatek)
	THEN
		RAISE EXCEPTION 'Glos nie zostal zlozony w czasie trwania wyborow';
	END IF;
	RETURN NEW;
END;
$$;


ALTER FUNCTION public.termin_glosowania() OWNER TO pkw;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: czlonkowie_komisji_obwodowych; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE czlonkowie_komisji_obwodowych (
    pesel character(11) NOT NULL,
    id_komisji integer NOT NULL,
    funkcja funkcja_w_komisji_obwodowej NOT NULL
);


ALTER TABLE public.czlonkowie_komisji_obwodowych OWNER TO pkw;

--
-- Name: czlonkowie_nadkomisji; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE czlonkowie_nadkomisji (
    pesel character(11) NOT NULL,
    id_nadkomisji integer NOT NULL,
    funkcja text
);


ALTER TABLE public.czlonkowie_nadkomisji OWNER TO pkw;

--
-- Name: glosy; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE glosy (
    obwod integer NOT NULL,
    id_kand integer NOT NULL,
    liczba integer NOT NULL,
    CONSTRAINT liczba_glosow_nieujemna CHECK ((liczba >= 0))
);


ALTER TABLE public.glosy OWNER TO pkw;

--
-- Name: glosy_niewazne; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE glosy_niewazne (
    obwod integer NOT NULL,
    id_nadkomisji integer NOT NULL,
    id_wyborow integer NOT NULL,
    liczba integer NOT NULL
);


ALTER TABLE public.glosy_niewazne OWNER TO pkw;

--
-- Name: kandydaci; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE kandydaci (
    id integer NOT NULL,
    id_listy integer NOT NULL,
    pesel character(11) NOT NULL
);


ALTER TABLE public.kandydaci OWNER TO pkw;

--
-- Name: kandydaci_id_seq; Type: SEQUENCE; Schema: public; Owner: pkw
--

CREATE SEQUENCE kandydaci_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kandydaci_id_seq OWNER TO pkw;

--
-- Name: kandydaci_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pkw
--

ALTER SEQUENCE kandydaci_id_seq OWNED BY kandydaci.id;


--
-- Name: komisje_obwodowe; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE komisje_obwodowe (
    id integer NOT NULL,
    nazwa text
);


ALTER TABLE public.komisje_obwodowe OWNER TO pkw;

--
-- Name: komisje_obwodowe_id_seq; Type: SEQUENCE; Schema: public; Owner: pkw
--

CREATE SEQUENCE komisje_obwodowe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.komisje_obwodowe_id_seq OWNER TO pkw;

--
-- Name: komisje_obwodowe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pkw
--

ALTER SEQUENCE komisje_obwodowe_id_seq OWNED BY komisje_obwodowe.id;


--
-- Name: komisje_obwodowe_nadkomisje; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE komisje_obwodowe_nadkomisje (
    id_komisji integer NOT NULL,
    id_nadkomisji integer NOT NULL
);


ALTER TABLE public.komisje_obwodowe_nadkomisje OWNER TO pkw;

--
-- Name: komitety; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE komitety (
    id integer NOT NULL,
    nazwa text NOT NULL,
    nr integer,
    CONSTRAINT numery_komitetow_dodatnie CHECK ((nr > 0))
);


ALTER TABLE public.komitety OWNER TO pkw;

--
-- Name: komitety_id_seq; Type: SEQUENCE; Schema: public; Owner: pkw
--

CREATE SEQUENCE komitety_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.komitety_id_seq OWNER TO pkw;

--
-- Name: komitety_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pkw
--

ALTER SEQUENCE komitety_id_seq OWNED BY komitety.id;


--
-- Name: listy; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE listy (
    id integer NOT NULL,
    id_komitetu integer NOT NULL,
    id_nadkomisji integer NOT NULL,
    id_wyborow integer NOT NULL
);


ALTER TABLE public.listy OWNER TO pkw;

--
-- Name: listy_id_seq; Type: SEQUENCE; Schema: public; Owner: pkw
--

CREATE SEQUENCE listy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.listy_id_seq OWNER TO pkw;

--
-- Name: listy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pkw
--

ALTER SEQUENCE listy_id_seq OWNED BY listy.id;


--
-- Name: mandaty; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE mandaty (
    id_nadkomisji integer NOT NULL,
    id_wyborow integer NOT NULL,
    nazwa text NOT NULL,
    liczba integer NOT NULL,
    CONSTRAINT liczba_mandatow_dodatnia CHECK ((liczba > 0))
);


ALTER TABLE public.mandaty OWNER TO pkw;

--
-- Name: nadkomisje; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE nadkomisje (
    id integer NOT NULL,
    nazwa text NOT NULL,
    id_nadkomisji integer
);


ALTER TABLE public.nadkomisje OWNER TO pkw;

--
-- Name: nadkomisje_id_seq; Type: SEQUENCE; Schema: public; Owner: pkw
--

CREATE SEQUENCE nadkomisje_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nadkomisje_id_seq OWNER TO pkw;

--
-- Name: nadkomisje_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pkw
--

ALTER SEQUENCE nadkomisje_id_seq OWNED BY nadkomisje.id;


--
-- Name: obywatele; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE obywatele (
    pesel character(11) NOT NULL,
    data_urodzenia date,
    nr_dowodu character(9),
    imie character varying NOT NULL,
    nazwisko character varying NOT NULL,
    miasto character varying NOT NULL,
    kod_pocztowy character(6) NOT NULL,
    ulica character varying NOT NULL,
    nr_domu integer NOT NULL,
    literka_domu character(1),
    nr_mieszkania integer,
    pozbawiony_praw boolean NOT NULL,
    CONSTRAINT nr_domu_dodatni CHECK ((nr_domu > 0))
);


ALTER TABLE public.obywatele OWNER TO pkw;

--
-- Name: obywatele_obwody; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE obywatele_obwody (
    pesel character(11) NOT NULL,
    id_komisji_obwodowej integer,
    id_wyborow integer NOT NULL,
    oddal_glos boolean NOT NULL
);


ALTER TABLE public.obywatele_obwody OWNER TO pkw;

--
-- Name: podpisy; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE podpisy (
    pesel character(11) NOT NULL,
    id_listy integer NOT NULL
);


ALTER TABLE public.podpisy OWNER TO pkw;

--
-- Name: typy_wyborow; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE typy_wyborow (
    nazwa text NOT NULL,
    liczba_podpisow integer NOT NULL,
    minimalny_wiek integer NOT NULL,
    sposob_liczenia_glosow ordynacja NOT NULL,
    CONSTRAINT liczba_podpisow_nieujemna CHECK ((liczba_podpisow >= 0)),
    CONSTRAINT minimalny_wiek_nieujemny CHECK ((minimalny_wiek >= 0))
);


ALTER TABLE public.typy_wyborow OWNER TO pkw;

--
-- Name: wybory; Type: TABLE; Schema: public; Owner: pkw; Tablespace: 
--

CREATE TABLE wybory (
    id integer NOT NULL,
    poczatek timestamp without time zone NOT NULL,
    koniec timestamp without time zone NOT NULL,
    typ text NOT NULL,
    nazwa text NOT NULL,
    CONSTRAINT wybory_zaczynaja_sie_nim_sie_skoncza CHECK ((poczatek < koniec))
);


ALTER TABLE public.wybory OWNER TO pkw;

--
-- Name: wybory_id_seq; Type: SEQUENCE; Schema: public; Owner: pkw
--

CREATE SEQUENCE wybory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wybory_id_seq OWNER TO pkw;

--
-- Name: wybory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pkw
--

ALTER SEQUENCE wybory_id_seq OWNED BY wybory.id;


--
-- Name: zatwierdzone_listy; Type: VIEW; Schema: public; Owner: pkw
--

CREATE VIEW zatwierdzone_listy AS
 SELECT listy.id,
    listy.id_komitetu,
    listy.id_nadkomisji,
    listy.id_wyborow
   FROM listy
  WHERE (listy.id IN ( SELECT l.id
           FROM ((((wybory w
             JOIN mandaty m ON ((w.id = m.id_wyborow)))
             JOIN listy l ON (((l.id_nadkomisji = m.id_nadkomisji) AND (l.id_wyborow = m.id_wyborow))))
             JOIN podpisy p ON ((l.id = p.id_listy)))
             JOIN typy_wyborow tw ON ((w.typ = tw.nazwa)))
          GROUP BY l.id, tw.liczba_podpisow
         HAVING (count(*) >= tw.liczba_podpisow)));


ALTER TABLE public.zatwierdzone_listy OWNER TO pkw;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY kandydaci ALTER COLUMN id SET DEFAULT nextval('kandydaci_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY komisje_obwodowe ALTER COLUMN id SET DEFAULT nextval('komisje_obwodowe_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY komitety ALTER COLUMN id SET DEFAULT nextval('komitety_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY listy ALTER COLUMN id SET DEFAULT nextval('listy_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY nadkomisje ALTER COLUMN id SET DEFAULT nextval('nadkomisje_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY wybory ALTER COLUMN id SET DEFAULT nextval('wybory_id_seq'::regclass);


--
-- Data for Name: czlonkowie_komisji_obwodowych; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY czlonkowie_komisji_obwodowych (pesel, id_komisji, funkcja) FROM stdin;
\.


--
-- Data for Name: czlonkowie_nadkomisji; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY czlonkowie_nadkomisji (pesel, id_nadkomisji, funkcja) FROM stdin;
90072803272	1	Najwyzszy i Niepodzielny Wladca
\.


--
-- Data for Name: glosy; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY glosy (obwod, id_kand, liczba) FROM stdin;
\.


--
-- Data for Name: glosy_niewazne; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY glosy_niewazne (obwod, id_nadkomisji, id_wyborow, liczba) FROM stdin;
\.


--
-- Data for Name: kandydaci; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY kandydaci (id, id_listy, pesel) FROM stdin;
1	1	50121803275
\.


--
-- Name: kandydaci_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pkw
--

SELECT pg_catalog.setval('kandydaci_id_seq', 1, true);


--
-- Data for Name: komisje_obwodowe; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY komisje_obwodowe (id, nazwa) FROM stdin;
1	Komisja Wygwizdow Gorny
\.


--
-- Name: komisje_obwodowe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pkw
--

SELECT pg_catalog.setval('komisje_obwodowe_id_seq', 1, true);


--
-- Data for Name: komisje_obwodowe_nadkomisje; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY komisje_obwodowe_nadkomisje (id_komisji, id_nadkomisji) FROM stdin;
1	1
\.


--
-- Data for Name: komitety; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY komitety (id, nazwa, nr) FROM stdin;
1	Komitet Zjednoczonej Wcalenierosji	1
\.


--
-- Name: komitety_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pkw
--

SELECT pg_catalog.setval('komitety_id_seq', 1, true);


--
-- Data for Name: listy; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY listy (id, id_komitetu, id_nadkomisji, id_wyborow) FROM stdin;
1	1	1	1
\.


--
-- Name: listy_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pkw
--

SELECT pg_catalog.setval('listy_id_seq', 1, true);


--
-- Data for Name: mandaty; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY mandaty (id_nadkomisji, id_wyborow, nazwa, liczba) FROM stdin;
1	1	Prezydent Rzeczypospolitej Polskiej	1
\.


--
-- Data for Name: nadkomisje; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY nadkomisje (id, nazwa, id_nadkomisji) FROM stdin;
1	Panstwowa Komisja Wyborcza w Wygwizdowie Gornym	\N
\.


--
-- Name: nadkomisje_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pkw
--

SELECT pg_catalog.setval('nadkomisje_id_seq', 1, true);


--
-- Data for Name: obywatele; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY obywatele (pesel, data_urodzenia, nr_dowodu, imie, nazwisko, miasto, kod_pocztowy, ulica, nr_domu, literka_domu, nr_mieszkania, pozbawiony_praw) FROM stdin;
90072803272	1990-07-28	AXM796429	Tomasz	Kisielewski	Krakow	30-222	Migdalowa	8	\N	\N	f
69080403273	1969-08-04	AXA796429	Komasz	Tisielewski	Mrakow	20-322	Kigdalowa	2	\N	\N	t
88070403273	1988-07-04	BMX796429	Euk	Medes	Ateny	16-304	Ateny	5	j	2	f
06083003277	2006-08-30	BMX796430	Archi	Lides	Ateny	16-304	Ateny	5	j	1	f
50121803275	1950-12-18	BMX796999	Ronislaw	Omorow	Wrecin	03-303	Wrecin	33	\N	\N	f
52070503271	1952-07-05	BMX796888	Lukasz	Szymanski	Ostrozanka	03-303	Ostrozanka	10	\N	\N	f
59011303279	1959-01-13	BMX796788	Kowal	Janski	Warszawa	11-111	Kosciuszki	11	c	12	f
\.


--
-- Data for Name: obywatele_obwody; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY obywatele_obwody (pesel, id_komisji_obwodowej, id_wyborow, oddal_glos) FROM stdin;
90072803272	1	1	f
69080403273	1	1	f
88070403273	1	1	f
06083003277	1	1	f
50121803275	1	1	f
52070503271	1	1	f
59011303279	1	1	f
\.


--
-- Data for Name: podpisy; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY podpisy (pesel, id_listy) FROM stdin;
50121803275	1
\.


--
-- Data for Name: typy_wyborow; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY typy_wyborow (nazwa, liczba_podpisow, minimalny_wiek, sposob_liczenia_glosow) FROM stdin;
Wybory Prezydenckie	100000	35	jednoosobowe
Wybory do Senatu	3000	30	wiekszosciowe
Wybory do Sejmu	5000	21	Dhondt
Wybory do Rady duzego miasta	500	18	Dhondt
Wybory do Rady mniejszego miasta	100	18	wiekszosciowe
Wybory do Rady dzielnicy/gminy	25	18	wiekszosciowe
Wybory na prezydenta miasta	500	21	jednoosobowe
Wybory na burmistrza miasta	100	21	jednoosobowe
\.


--
-- Data for Name: wybory; Type: TABLE DATA; Schema: public; Owner: pkw
--

COPY wybory (id, poczatek, koniec, typ, nazwa) FROM stdin;
1	2015-05-10 06:00:00	2015-05-10 20:00:00	Wybory Prezydenckie	Wybory Prezydenckie 2015
2	2015-03-10 07:00:00	2015-03-10 21:00:00	Wybory do Senatu	Wybory do Senatu 2015
3	2015-03-10 07:00:00	2015-03-10 21:00:00	Wybory do Sejmu	Wybory do Sejmu 2015
\.


--
-- Name: wybory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pkw
--

SELECT pg_catalog.setval('wybory_id_seq', 3, true);


--
-- Name: czlonkowie_komisji_obwodowych_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY czlonkowie_komisji_obwodowych
    ADD CONSTRAINT czlonkowie_komisji_obwodowych_pkey PRIMARY KEY (pesel, id_komisji);


--
-- Name: czlonkowie_nadkomisji_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY czlonkowie_nadkomisji
    ADD CONSTRAINT czlonkowie_nadkomisji_pkey PRIMARY KEY (pesel, id_nadkomisji);


--
-- Name: glosy_niewazne_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY glosy_niewazne
    ADD CONSTRAINT glosy_niewazne_pkey PRIMARY KEY (obwod, id_nadkomisji, id_wyborow);


--
-- Name: glosy_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY glosy
    ADD CONSTRAINT glosy_pkey PRIMARY KEY (obwod, id_kand);


--
-- Name: kandydaci_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY kandydaci
    ADD CONSTRAINT kandydaci_pkey PRIMARY KEY (id);


--
-- Name: komisje_obwodowe_nadkomisje_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY komisje_obwodowe_nadkomisje
    ADD CONSTRAINT komisje_obwodowe_nadkomisje_pkey PRIMARY KEY (id_komisji, id_nadkomisji);


--
-- Name: komisje_obwodowe_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY komisje_obwodowe
    ADD CONSTRAINT komisje_obwodowe_pkey PRIMARY KEY (id);


--
-- Name: komitety_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY komitety
    ADD CONSTRAINT komitety_pkey PRIMARY KEY (id);


--
-- Name: listy_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY listy
    ADD CONSTRAINT listy_pkey PRIMARY KEY (id);


--
-- Name: mandaty_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY mandaty
    ADD CONSTRAINT mandaty_pkey PRIMARY KEY (id_nadkomisji, id_wyborow);


--
-- Name: nadkomisje_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY nadkomisje
    ADD CONSTRAINT nadkomisje_pkey PRIMARY KEY (id);


--
-- Name: obywatele_nr_dowodu_key; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY obywatele
    ADD CONSTRAINT obywatele_nr_dowodu_key UNIQUE (nr_dowodu);


--
-- Name: obywatele_obwody_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY obywatele_obwody
    ADD CONSTRAINT obywatele_obwody_pkey PRIMARY KEY (pesel, id_wyborow);


--
-- Name: obywatele_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY obywatele
    ADD CONSTRAINT obywatele_pkey PRIMARY KEY (pesel);


--
-- Name: podpisy_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY podpisy
    ADD CONSTRAINT podpisy_pkey PRIMARY KEY (pesel, id_listy);


--
-- Name: typy_wyborow_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY typy_wyborow
    ADD CONSTRAINT typy_wyborow_pkey PRIMARY KEY (nazwa);


--
-- Name: wybory_pkey; Type: CONSTRAINT; Schema: public; Owner: pkw; Tablespace: 
--

ALTER TABLE ONLY wybory
    ADD CONSTRAINT wybory_pkey PRIMARY KEY (id);


--
-- Name: edycja_typu_wyborow; Type: TRIGGER; Schema: public; Owner: pkw
--

CREATE TRIGGER edycja_typu_wyborow BEFORE UPDATE ON typy_wyborow FOR EACH ROW EXECUTE PROCEDURE edycja_typu_wyborow();


--
-- Name: jedna_lista; Type: TRIGGER; Schema: public; Owner: pkw
--

CREATE TRIGGER jedna_lista BEFORE INSERT ON kandydaci FOR EACH ROW EXECUTE PROCEDURE jedna_lista();


--
-- Name: prawa_kandydatow; Type: TRIGGER; Schema: public; Owner: pkw
--

CREATE TRIGGER prawa_kandydatow BEFORE INSERT OR UPDATE ON kandydaci FOR EACH ROW EXECUTE PROCEDURE prawa_kandydatow();


--
-- Name: prawa_wyborcow; Type: TRIGGER; Schema: public; Owner: pkw
--

CREATE TRIGGER prawa_wyborcow BEFORE INSERT OR UPDATE ON obywatele_obwody FOR EACH ROW EXECUTE PROCEDURE prawa_wyborcow();


--
-- Name: sprawdz_czlonkow_komisji; Type: TRIGGER; Schema: public; Owner: pkw
--

CREATE TRIGGER sprawdz_czlonkow_komisji BEFORE INSERT ON czlonkowie_komisji_obwodowych FOR EACH ROW EXECUTE PROCEDURE sprawdz_czlonkow_komisji();


--
-- Name: sprawdz_czlonkow_nadkomisji; Type: TRIGGER; Schema: public; Owner: pkw
--

CREATE TRIGGER sprawdz_czlonkow_nadkomisji BEFORE INSERT OR UPDATE ON czlonkowie_nadkomisji FOR EACH ROW EXECUTE PROCEDURE sprawdz_czlonkow_nadkomisji();


--
-- Name: sprawdz_pesel; Type: TRIGGER; Schema: public; Owner: pkw
--

CREATE TRIGGER sprawdz_pesel BEFORE INSERT OR UPDATE ON obywatele FOR EACH ROW EXECUTE PROCEDURE sprawdz_pesel();


--
-- Name: sprawdz_prawa; Type: TRIGGER; Schema: public; Owner: pkw
--

CREATE TRIGGER sprawdz_prawa BEFORE UPDATE ON obywatele FOR EACH ROW EXECUTE PROCEDURE sprawdz_prawa();


--
-- Name: termin_glosowania; Type: TRIGGER; Schema: public; Owner: pkw
--

CREATE TRIGGER termin_glosowania BEFORE INSERT ON glosy FOR EACH ROW EXECUTE PROCEDURE termin_glosowania();


--
-- Name: czlonkowie_komisji_obwodowych_id_komisji_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY czlonkowie_komisji_obwodowych
    ADD CONSTRAINT czlonkowie_komisji_obwodowych_id_komisji_fkey FOREIGN KEY (id_komisji) REFERENCES komisje_obwodowe(id);


--
-- Name: czlonkowie_komisji_obwodowych_pesel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY czlonkowie_komisji_obwodowych
    ADD CONSTRAINT czlonkowie_komisji_obwodowych_pesel_fkey FOREIGN KEY (pesel) REFERENCES obywatele(pesel);


--
-- Name: czlonkowie_nadkomisji_id_nadkomisji_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY czlonkowie_nadkomisji
    ADD CONSTRAINT czlonkowie_nadkomisji_id_nadkomisji_fkey FOREIGN KEY (id_nadkomisji) REFERENCES nadkomisje(id);


--
-- Name: czlonkowie_nadkomisji_pesel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY czlonkowie_nadkomisji
    ADD CONSTRAINT czlonkowie_nadkomisji_pesel_fkey FOREIGN KEY (pesel) REFERENCES obywatele(pesel);


--
-- Name: glosy_id_kand_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY glosy
    ADD CONSTRAINT glosy_id_kand_fkey FOREIGN KEY (id_kand) REFERENCES kandydaci(id);


--
-- Name: glosy_niewazne_id_nadkomisji_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY glosy_niewazne
    ADD CONSTRAINT glosy_niewazne_id_nadkomisji_fkey FOREIGN KEY (id_nadkomisji, id_wyborow) REFERENCES mandaty(id_nadkomisji, id_wyborow);


--
-- Name: glosy_niewazne_obwod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY glosy_niewazne
    ADD CONSTRAINT glosy_niewazne_obwod_fkey FOREIGN KEY (obwod) REFERENCES komisje_obwodowe(id);


--
-- Name: glosy_obwod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY glosy
    ADD CONSTRAINT glosy_obwod_fkey FOREIGN KEY (obwod) REFERENCES komisje_obwodowe(id);


--
-- Name: kandydaci_id_listy_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY kandydaci
    ADD CONSTRAINT kandydaci_id_listy_fkey FOREIGN KEY (id_listy) REFERENCES listy(id);


--
-- Name: kandydaci_pesel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY kandydaci
    ADD CONSTRAINT kandydaci_pesel_fkey FOREIGN KEY (pesel) REFERENCES obywatele(pesel);


--
-- Name: komisje_obwodowe_nadkomisje_id_komisji_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY komisje_obwodowe_nadkomisje
    ADD CONSTRAINT komisje_obwodowe_nadkomisje_id_komisji_fkey FOREIGN KEY (id_komisji) REFERENCES komisje_obwodowe(id);


--
-- Name: komisje_obwodowe_nadkomisje_id_nadkomisji_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY komisje_obwodowe_nadkomisje
    ADD CONSTRAINT komisje_obwodowe_nadkomisje_id_nadkomisji_fkey FOREIGN KEY (id_nadkomisji) REFERENCES nadkomisje(id);


--
-- Name: listy_id_komitetu_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY listy
    ADD CONSTRAINT listy_id_komitetu_fkey FOREIGN KEY (id_komitetu) REFERENCES komitety(id);


--
-- Name: listy_id_nadkomisji_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY listy
    ADD CONSTRAINT listy_id_nadkomisji_fkey FOREIGN KEY (id_nadkomisji, id_wyborow) REFERENCES mandaty(id_nadkomisji, id_wyborow);


--
-- Name: mandaty_id_nadkomisji_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY mandaty
    ADD CONSTRAINT mandaty_id_nadkomisji_fkey FOREIGN KEY (id_nadkomisji) REFERENCES nadkomisje(id);


--
-- Name: mandaty_id_wyborow_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY mandaty
    ADD CONSTRAINT mandaty_id_wyborow_fkey FOREIGN KEY (id_wyborow) REFERENCES wybory(id);


--
-- Name: nadkomisje_id_nadkomisji_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY nadkomisje
    ADD CONSTRAINT nadkomisje_id_nadkomisji_fkey FOREIGN KEY (id_nadkomisji) REFERENCES nadkomisje(id);


--
-- Name: obywatele_obwody_id_komisji_obwodowej_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY obywatele_obwody
    ADD CONSTRAINT obywatele_obwody_id_komisji_obwodowej_fkey FOREIGN KEY (id_komisji_obwodowej) REFERENCES komisje_obwodowe(id);


--
-- Name: obywatele_obwody_id_wyborow_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY obywatele_obwody
    ADD CONSTRAINT obywatele_obwody_id_wyborow_fkey FOREIGN KEY (id_wyborow) REFERENCES wybory(id);


--
-- Name: obywatele_obwody_pesel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY obywatele_obwody
    ADD CONSTRAINT obywatele_obwody_pesel_fkey FOREIGN KEY (pesel) REFERENCES obywatele(pesel);


--
-- Name: podpisy_id_listy_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY podpisy
    ADD CONSTRAINT podpisy_id_listy_fkey FOREIGN KEY (id_listy) REFERENCES listy(id);


--
-- Name: podpisy_pesel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY podpisy
    ADD CONSTRAINT podpisy_pesel_fkey FOREIGN KEY (pesel) REFERENCES obywatele(pesel);


--
-- Name: wybory_typ_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pkw
--

ALTER TABLE ONLY wybory
    ADD CONSTRAINT wybory_typ_fkey FOREIGN KEY (typ) REFERENCES typy_wyborow(nazwa);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

