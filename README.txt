Aplikacja do obsługi wyborów w Rzeczypospolitej Polskiej
=======================================================

Ogólny zamysł
-------------

Chcemy stworzyć pełną aplikację do obsługi demokratycznych wyborów zgodnych z
ordynacją w Rzeczypospolitej Polskiej. Będzie ona pozwalała na stworzenie nowych
wyborów, ustalenie składów komisji, zarejestrowanie komitetów, stworzenie list
wyborczych i zebranie podpisów pod nimi, wygenerowanie kart do głosowania,
przeliczenie głosów oraz rozdzielenie mandatów.

Realizacja etapów przeprowadzania wyborów
-----------------------------------------

### Wybory

Wybory będzie się tworzyło wybierając typ wyborów do stworzenia, datę początku,
datę końca. Typ wyborów rozróżnia między wyborami na różne stanowiska, z niego i
dat można też wygenerować nazwę wyborów, np. Wybory Prezydenckie 2015.

### Komisje wyborcze

Okręgowym komisjom wyborczym będzie trzeba przydzielić członków i odpowiednie
nadkomisje dla danych wyborów. Do komisji okręgowych będą przypisani uprawnieni
w nich do głosowania w danych wyborach -- pobranie zezwolenia na głosowanie poza
miejscem zamieszkania będzie reprezentowane przez NULL w tamtym miejscu.
Nadkomisjom (odpowiadającym komisjom okręgowym, terytorialnym bądź państwowym)
będzie można przydzielić członków i odpowiednie wyższe nadkomisje, a właściwym
będzie należało przydzielić również mandaty do zdobycia w danych wyborach.

### Komitety i listy wyborcze

Będzie można zarejestrować komitet o danej nazwie, a każdy z komitetów będzie mógł zgłaszać
listy wyborcze zawierające obywateli uprawnionych do starania się o dane
mandaty. Będzie można komitetom przyznać numery. Następnie będzie można zbierać
podpisy poparcia dla konkretnych list i na podstawie tego stwierdzać, czy udało
się je zarejestrować.

### Karty do głosowania

Karty będą generowane w danej komisji obwodowej na podstawie list wyborczych
zarejestrowanych we wszystkich jej nadkomisjach. W praktyce wygeneruje to kilka
kart jeśli różne nadkomisje mają różne mandaty i rozróżni odbywające się
jednocześnie np. wybory samorządowe do samorządów różnego stopnia.

### Przeliczenie głosów

Głosy będą zliczane w danych komisjach obwodowych dla danych kandydatów. W
każdej komisji będzie sztuczna lista, nie pokazująca się na kartach do
głosowania, zawierająca jednego kandydata symbolizującego głos nieważny.

### Przyznawanie mandatów

Mandaty będą przyznawane na podstawie liczby zebranych głosów w sposób zależny
od typu wyborów. Będą też rozwiązywane wszystkie problemy z kandydatami którzy
zmarli, wygrali kilka wyborów jednocześnie bądź wygrali wybory już będąc
wybranymi gdzie indziej.

Instalacja
----------

Najprościej jest najpierw zainstalować i skonfigurować język go, a następnie
wykonać po kolei:

`go get bitbucket.org/timorl/pekawu`

`cd $GOPATH/src/bitbucket.org/timorl/pekawu`

Teraz warto dodać użytkownika pkw bez hasła do bazy danych i uruchomić `psql -U
pkw < sql/create.sql`. Mamy w ten sposób bazę.

Aby skompilować i uruchomić aplikację, należy wykonać:

`go build`

`./pekawu`

To uruchamia dwa serwery http na lokalnej maszynie na portach 8080 i 8181 dla
administratorów i głosujących uczestników odpowiednio. Łączyć się z tymi
serwerami można za pomocą dowolnej przeglądarki, ale aplikacja najlepiej działa
w chromium.
