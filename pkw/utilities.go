package pkw

import (
	"database/sql"
	_ "github.com/lib/pq"
)

func OpenDatabase() (*sql.DB, error) {
	return sql.Open("postgres", "user=pkw dbname=pkw sslmode=disable")
}
