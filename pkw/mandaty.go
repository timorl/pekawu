package pkw

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"sort"
	"strconv"
)

type Mandaty struct {
	IdNadkomisji int
	IdWyborow    int
	Nazwa        string
	Liczba       int
}

type Divisor struct {
	IdListy int
	Score   float32
}

type By func(p1, p2 *Divisor) bool

func (by By) Sort(divisors []Divisor) {
	ps := &divisorSorter{
		divisors: divisors,
		by:       by, // The Sort method's receiver is the function (closure) that defines the sort order.
	}
	sort.Sort(ps)
}

type divisorSorter struct {
	divisors []Divisor
	by       func(p1, p2 *Divisor) bool // Closure used in the Less method.
}

func (s *divisorSorter) Len() int {
	return len(s.divisors)
}

func (s *divisorSorter) Swap(i, j int) {
	s.divisors[i], s.divisors[j] = s.divisors[j], s.divisors[i]
}

func (s *divisorSorter) Less(i, j int) bool {
	return s.by(&s.divisors[i], &s.divisors[j])
}

func decreasingScore(p1, p2 *Divisor) bool {
	return (p1.Score > p2.Score)
}

func MandatyFromForm(form map[string][]string) (Mandaty, error) {
	var result Mandaty
	if val, ok := form["nazwa"]; ok {
		result.Nazwa = val[0]
	} else {
		return result, errors.New("Brak nazwy")
	}
	if val, ok := form["id_nadkomisji"]; ok {
		var err error
		idNdk, err := strconv.ParseInt(val[0], 0, 0)
		if err != nil {
			return result, errors.New("Brak nadkomisji")
		}
		result.IdNadkomisji = int(idNdk)
	} else {
		return result, errors.New("Brak nadkomisji")
	}
	if val, ok := form["id_wyborow"]; ok {
		var err error
		idWbr, err := strconv.ParseInt(val[0], 0, 0)
		if err != nil {
			return result, errors.New("Brak wyborów")
		}
		result.IdWyborow = int(idWbr)
	} else {
		return result, errors.New("Brak wyborów")
	}
	if val, ok := form["liczba"]; ok {
		lczb, err := strconv.ParseInt(val[0], 0, 0)
		if err != nil {
			return result, errors.New("Brak liczby")
		}
		result.Liczba = int(lczb)
	} else {
		return result, errors.New("Brak liczby")
	}
	return result, nil
}

func AddMandaty(m Mandaty) bool {
	db, err := OpenDatabase()

	if err != nil {
		return false
	}
	_, err = db.Exec("INSERT INTO mandaty(id_nadkomisji, id_wyborow, nazwa, liczba) VALUES ($1, $2, $3, $4)", m.IdNadkomisji, m.IdWyborow, m.Nazwa, m.Liczba)

	if err != nil {
		return false
	}
	return true
}

func GetMandaty() []Mandaty {
	var result []Mandaty
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT id_nadkomisji, id_wyborow, nazwa, liczba FROM mandaty")

	if err != nil {
		panic(err)
	}

	var temporaryMandaty Mandaty

	for rows.Next() {

		err = rows.Scan(&temporaryMandaty.IdNadkomisji, &temporaryMandaty.IdWyborow, &temporaryMandaty.Nazwa, &temporaryMandaty.Liczba)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryMandaty)
	}
	return result
}

func GetMandatyOfGivenWybory(givenId int) []Mandaty {
	var result []Mandaty
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT id_nadkomisji, id_wyborow, nazwa, liczba FROM mandaty WHERE id_wyborow=$1", givenId)

	if err != nil {
		panic(err)
	}

	var temporaryMandaty Mandaty

	for rows.Next() {

		err = rows.Scan(&temporaryMandaty.IdNadkomisji, &temporaryMandaty.IdWyborow, &temporaryMandaty.Nazwa, &temporaryMandaty.Liczba)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryMandaty)
	}
	return result
}

func WhoGot(IdWyborow, IdNadkomisji int) []Obywatel {
	var result []Obywatel
	var typWyborow string
	var rows *sql.Rows
	var err error
	var temporaryObywatel Obywatel

	db, err := OpenDatabase()

	rows, err = db.Query("SELECT tw.sposob_liczenia_glosow FROM wybory w"+
		" JOIN typy_wyborow tw"+
		" ON w.typ=tw.nazwa WHERE w.id=$1", IdWyborow)
	if err != nil {
		panic(err)
	}

	rows.Next()
	err = rows.Scan(&typWyborow)
	if err != nil {
		panic(err)
	}

	var liczbaGlosowWaznych int
	rows, err = db.Query("SELECT SUM(g.liczba)"+
		" FROM glosy g"+
		" JOIN kandydaci k"+
		" ON g.id_kand = k.id"+
		" JOIN listy l"+
		" ON k.id_listy = l.id"+
		" JOIN mandaty m"+
		" ON m.id_nadkomisji = l.id_nadkomisji AND m.id_wyborow = l.id_wyborow"+
		" WHERE m.id_wyborow=$1", IdWyborow)
	if err != nil {
		panic(err)
	}

	rows.Next()
	err = rows.Scan(&liczbaGlosowWaznych)
	if err != nil {
		panic(err)
	}

	if typWyborow == "jednoosobowe" {

		var maxLiczbaGlosow int
		var secondLiczbaGlosow int
		var maxMultiplicity int
		rows, err = db.Query("SELECT MAX(g.liczba)"+
			" FROM glosy g"+
			" JOIN kandydaci k"+
			" ON g.id_kand = k.id"+
			" JOIN listy l"+
			" ON k.id_listy = l.id"+
			" WHERE l.id_wyborow=$1", IdWyborow)
		if err != nil {
			panic(err)
		}
		err = rows.Scan(&maxLiczbaGlosow)
		if err != nil {
			panic(err)
		}

		rows, err = db.Query("SELECT MAX(g.liczba)"+
			" FROM glosy g"+
			" JOIN kandydaci k"+
			" ON g.id_kand = k.id"+
			" JOIN listy l"+
			" ON k.id_listy = l.id"+
			" WHERE l.id_wyborow=$1"+
			" AND g.liczba != $2", IdWyborow, maxLiczbaGlosow)
		if err != nil {
			panic(err)
		}
		err = rows.Scan(&secondLiczbaGlosow)
		if err != nil {
			panic(err)
		}

		rows, err = db.Query("SELECT COUNT(*)"+
			" FROM glosy g"+
			" JOIN kandydaci k"+
			" ON g.id_kand = k.id"+
			" JOIN listy l"+
			" ON k.id_listy = l.id"+
			" WHERE l.id_wyborow=$1"+
			" AND g.liczba = $2", IdWyborow, maxLiczbaGlosow)
		if err != nil {
			panic(err)
		}
		err = rows.Scan(&maxMultiplicity)
		if err != nil {
			panic(err)
		}

		if (2*maxLiczbaGlosow) > liczbaGlosowWaznych || (maxMultiplicity > 1) {
			rows, err = db.Query("SELECT k.pesel, k.data_urodzenia, k.nr_dowodu, k.imie, k.nazwisko, k.miasto, k.kod_pocztowy, k.ulica,"+
				" k.nr_domu, k.literka_domu, k.nr_mieszkania, k.pozbawiony_praw"+
				" FROM glosy g"+
				" JOIN kandydaci k"+
				" ON g.id_kand = k.id"+
				" JOIN listy l"+
				" ON k.id_listy = l.id"+
				" WHERE l.id_wyborow=$1"+
				" AND g.liczba =$2", IdWyborow, maxLiczbaGlosow)
			if err != nil {
				panic(err)
			}
		} else {

			rows, err = db.Query("SELECT k.pesel, k.data_urodzenia, k.nr_dowodu, k.imie, k.nazwisko, k.miasto, k.kod_pocztowy, k.ulica,"+
				" k.nr_domu, k.literka_domu, k.nr_mieszkania, k.pozbawiony_praw"+
				" FROM glosy g"+
				" JOIN kandydaci k"+
				" ON g.id_kand = k.id"+
				" JOIN listy l"+
				" ON k.id_listy = l.id"+
				" WHERE l.id_wyborow=$1"+
				" AND (g.liczba =$2"+
				" OR g.liczba =$3)", IdWyborow, maxLiczbaGlosow, secondLiczbaGlosow)
			if err != nil {
				panic(err)
			}

		}

		for rows.Next() {
			err = rows.Scan(&temporaryObywatel.Pesel, &temporaryObywatel.Data_urodzenia,
				&temporaryObywatel.Nr_dowodu, &temporaryObywatel.Imie,
				&temporaryObywatel.Nazwisko, &temporaryObywatel.Miasto,
				&temporaryObywatel.Kod_pocztowy, &temporaryObywatel.Ulica,
				&temporaryObywatel.Nr_domu, &temporaryObywatel.Literka_domu,
				&temporaryObywatel.Nr_mieszkania, &temporaryObywatel.Pozbawiony_praw)
			if err != nil {
				panic(err)
			}
			result = append(result, temporaryObywatel)
		}

	} else if typWyborow == "wiekszosciowe" {

		var liczbaMandatow int
		var maxLiczbaGlosow int
		var ileGlosowTrzeba int
		var liczbaZwyciezcow int
		rows, err = db.Query("SELECT m.liczba"+
			" FROM  nadkomisje n"+
			" JOIN mandaty m"+
			" ON m.id_nadkomisji = n.id"+
			" WHERE m.id_nadkomisji=$1", IdNadkomisji)
		if err != nil {
			panic(err)
		}
		err = rows.Scan(&liczbaMandatow)
		if err != nil {
			panic(err)
		}

		rows, err = db.Query("SELECT MAX(g.liczba)"+
			" FROM glosy g"+
			" JOIN kandydaci k"+
			" ON g.id_kand = k.id"+
			" JOIN listy l"+
			" ON k.id_listy = l.id"+
			" JOIN komitety ko"+
			" ON l.id_komitetu = ko.id"+
			" WHERE l.id_nadkomisji = $1 AND l.id_wyborow = $2", IdNadkomisji, IdWyborow)
		if err != nil {
			panic(err)
		}
		err = rows.Scan(&maxLiczbaGlosow)
		if err != nil {
			panic(err)
		}
		ileGlosowTrzeba = maxLiczbaGlosow

		rows, err = db.Query("SELECT COUNT(*)"+
			" FROM glosy g"+
			" JOIN kandydaci k"+
			" ON g.id_kand = k.id"+
			" JOIN listy l"+
			" ON k.id_listy = l.id"+
			" WHERE l.id_nadkomisji = $1 AND l.id_wyborow = $2"+
			" AND g.liczba >=ileGlosowTrzeba", IdNadkomisji, IdWyborow)
		if err != nil {
			panic(err)
		}
		err = rows.Scan(&liczbaZwyciezcow)
		if err != nil {
			panic(err)
		}

		for liczbaZwyciezcow < liczbaMandatow {

			rows, err = db.Query("SELECT MAX(g.liczba)"+
				" FROM glosy g"+
				" JOIN kandydaci k"+
				" ON g.id_kand = k.id"+
				" JOIN listy l"+
				" ON k.id_listy = l.id"+
				" WHERE l.id_nadkomisji = $1 AND l.id_wyborow = $2"+
				" AND g.liczba < $3", IdNadkomisji, IdWyborow, ileGlosowTrzeba)
			if err != nil {
				panic(err)
			}
			err = rows.Scan(&ileGlosowTrzeba)
			if err != nil {
				panic(err)
			}

			rows, err = db.Query("SELECT COUNT(*)"+
				" FROM glosy g"+
				" JOIN kandydaci k"+
				" ON g.id_kand = k.id"+
				" JOIN listy l"+
				" ON k.id_listy = l.id"+
				" WHERE l.id_nadkomisji = $1 AND l.id_wyborow = $2"+
				" AND g.liczba >=$3", IdNadkomisji, IdWyborow, ileGlosowTrzeba)
			if err != nil {
				panic(err)
			}
			err = rows.Scan(&liczbaZwyciezcow)
			if err != nil {
				panic(err)
			}

		}

		rows, err = db.Query("SELECT k.pesel, k.data_urodzenia, k.nr_dowodu, k.imie, k.nazwisko, k.miasto, k.kod_pocztowy, k.ulica,"+
			" k.nr_domu, k.literka_domu, k.nr_mieszkania, k.pozbawiony_praw"+
			" FROM glosy g"+
			" JOIN kandydaci k"+
			" ON g.id_kand = k.id"+
			" JOIN listy l"+
			" ON k.id_listy = l.id"+
			" WHERE l.id_nadkomisji = $1 AND l.id_wyborow = $2"+
			" ORDER BY g.liczba DESC"+
			" LIMIT $3", IdNadkomisji, IdWyborow, liczbaZwyciezcow)
		if err != nil {
			panic(err)
		}

		for rows.Next() {
			err = rows.Scan(&temporaryObywatel.Pesel, &temporaryObywatel.Data_urodzenia,
				&temporaryObywatel.Nr_dowodu, &temporaryObywatel.Imie,
				&temporaryObywatel.Nazwisko, &temporaryObywatel.Miasto,
				&temporaryObywatel.Kod_pocztowy, &temporaryObywatel.Ulica,
				&temporaryObywatel.Nr_domu, &temporaryObywatel.Literka_domu,
				&temporaryObywatel.Nr_mieszkania, &temporaryObywatel.Pozbawiony_praw)
			if err != nil {
				panic(err)
			}
			result = append(result, temporaryObywatel)
		}

	} else if typWyborow == "Dhondt" {

		var LiczbaWaznychGlosow int
		var liczbaMandatow int
		var dzielniki []Divisor

		rows, err = db.Query("SELECT m.liczba"+
			" FROM  nadkomisje n"+
			" JOIN mandaty m"+
			" ON m.id_nadkomisji = n.id"+
			" WHERE m.id_nadkomisji=$1 AND m.id_wyborow = $2", IdNadkomisji, IdWyborow)
		if err != nil {
			panic(err)
		}
		rows.Next()
		err = rows.Scan(&liczbaMandatow)
		if err != nil {
			panic(err)
		}

		for i := 1; i <= liczbaMandatow; i++ {
			rows, err = db.Query("SELECT  SUM(g.liczba), l.id"+
				" FROM glosy g"+
				" JOIN kandydaci k"+
				" ON g.id_kand = k.id"+
				" JOIN listy l"+
				" ON k.id_listy = l.id"+
				" JOIN komitety ko"+
				" ON l.id_komitetu = ko.id"+
				" WHERE l.id_nadkomisji = $1 AND l.id_wyborow = $2"+
				" AND $3 < 20 * (SELECT SUM(gg.liczba) FROM glosy gg"+
				" JOIN kandydaci kk"+
				" ON gg.id_kand = kk.id"+
				" JOIN listy ll"+
				" ON kk.id_listy = ll.id"+
				" JOIN komitety koko"+
				" ON ll.id_komitetu = koko.id"+
				" WHERE ll.id_wyborow = $2"+
				" AND koko.id=ko.id)"+
				" GROUP BY l.id"+
				" HAVING COUNT(*)>=$4", IdNadkomisji, IdWyborow, LiczbaWaznychGlosow, i)
			if err != nil {
				panic(err)
			}

			var temporaryDivisor Divisor

			for rows.Next() {
				err = rows.Scan(&temporaryDivisor.Score, &temporaryDivisor.IdListy)
				if err != nil {
					panic(err)
				}
				temporaryDivisor.Score = temporaryDivisor.Score / (float32(i))
				dzielniki = append(dzielniki, temporaryDivisor)
			}
		}

		By(decreasingScore).Sort(dzielniki)

		for i := 0; i < liczbaMandatow; i++ {
			var currentIndex int
			currentIndex = 0
			if dzielniki[i].IdListy != -1 {
				currentIndex = 1
				for j := i + 1; j < liczbaMandatow; j++ {
					if dzielniki[j].IdListy == dzielniki[i].IdListy {
						dzielniki[j].IdListy = -1
						currentIndex++
					}
				}

				rows, err = db.Query("SELECT o.pesel, o.data_urodzenia, o.nr_dowodu, o.imie, o.nazwisko, o.miasto, o.kod_pocztowy, o.ulica,"+
					" o.nr_domu, o.literka_domu, o.nr_mieszkania, o.pozbawiony_praw"+
					" FROM glosy g"+
					" JOIN kandydaci k"+
					" ON g.id_kand = k.id"+
					" JOIN obywatele o"+
					" ON k.pesel = o.pesel"+
					" JOIN listy l"+
					" ON k.id_listy = l.id"+
					" WHERE k.id_listy=$1"+
					" ORDER BY g.liczba DESC"+
					" LIMIT $2", dzielniki[i].IdListy, currentIndex)
				if err != nil {
					panic(err)
				}

				for rows.Next() {
					err = rows.Scan(&temporaryObywatel.Pesel, &temporaryObywatel.Data_urodzenia,
						&temporaryObywatel.Nr_dowodu, &temporaryObywatel.Imie,
						&temporaryObywatel.Nazwisko, &temporaryObywatel.Miasto,
						&temporaryObywatel.Kod_pocztowy, &temporaryObywatel.Ulica,
						&temporaryObywatel.Nr_domu, &temporaryObywatel.Literka_domu,
						&temporaryObywatel.Nr_mieszkania, &temporaryObywatel.Pozbawiony_praw)
					if err != nil {
						panic(err)
					}
					result = append(result, temporaryObywatel)
				}
			}
		}
	}

	return result
}

func GetResultsOfGivenMandat(IdWyborow, IdNadkomisji int) []Wynik {
	var rows *sql.Rows
	var err error
	var result []Wynik

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT o.imie, o.nazwisko, kom.nazwa, coalesce(g.liczba,0)"+
		" FROM komisje_obwodowe ko"+
		" JOIN komisje_obwodowe_nadkomisje kon ON ko.id = kon.id_komisji"+
		" JOIN nadkomisje n ON n.id = kon.id_nadkomisji"+
		" JOIN listy l ON l.id_nadkomisji = n.id"+
		" JOIN komitety kom ON l.id_komitetu = kom.id"+
		" JOIN kandydaci k ON k.id_listy = l.id"+
		" JOIN obywatele o ON o.pesel = k.pesel"+
		" LEFT OUTER JOIN glosy g ON g.id_kand=k.id"+
		" WHERE l.id_wyborow=$1 AND kon.id_komisji=$2 ORDER BY 3 DESC", IdWyborow, IdNadkomisji)
	if err != nil {
		panic(err)
	}

	var temporaryWynik Wynik

	for rows.Next() {
		err = rows.Scan(&temporaryWynik.Imie, &temporaryWynik.Nazwisko, &temporaryWynik.NazwaKomitetu, &temporaryWynik.Glosy)
		if err != nil {
			panic(err)
		}
		result = append(result, temporaryWynik)
	}

	var liczbaNiewaznych int
	rows, err = db.Query("SELECT  count(*)"+
		" FROM glosy_niewazne"+
		" WHERE id_wyborow=$1 AND id_nadkomisji=$2", IdWyborow, IdNadkomisji)
	if err != nil {
		panic(err)
	}

	for rows.Next() {
		err = rows.Scan(&liczbaNiewaznych)
		if err != nil {
			panic(err)
		}
	}

	if liczbaNiewaznych > 0 {
		rows, err = db.Query("SELECT  liczba"+
			" FROM glosy_niewazne"+
			" WHERE id_wyborow=$1 AND id_nadkomisji=$2", IdWyborow, IdNadkomisji)
		if err != nil {
			panic(err)
		}

		for rows.Next() {
			err = rows.Scan(&liczbaNiewaznych)
			if err != nil {
				panic(err)
			}
		}
	}

	temporaryWynik.Imie = "Glos"
	temporaryWynik.Nazwisko = "Niewazny"
	temporaryWynik.NazwaKomitetu = "-"
	temporaryWynik.Glosy = liczbaNiewaznych
	result = append(result, temporaryWynik)

	return result
}
