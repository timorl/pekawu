package pkw

import (
	"database/sql"
	_ "github.com/lib/pq"
)

type KomisjaObwodowaNadkomisja struct {
	IdNadkomisji	int
	IdKomisji		int
}

func AddKomisjaObwodowaNadkomisja(kon KomisjaObwodowaNadkomisja) bool {

	db, err := OpenDatabase()
	
	if err != nil {
		panic(err)
		return false
	}
	_, err = db.Exec("INSERT INTO komisje_obwodowe_nadkomisje VALUES ($1, $2)", kon.IdNadkomisji, kon.IdKomisji);
	
	if err != nil {
		panic(err)
		return false
	}
	return true
}

func GetKomisjeObwodoweNadkomisje() []KomisjaObwodowaNadkomisja {

	var result []KomisjaObwodowaNadkomisja
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM komisje_obwodowe_nadkomisje")
	
	if err != nil {
		panic(err)
	}

	var temporaryKomisjaObwodowaNadkomisja KomisjaObwodowaNadkomisja

	for rows.Next() {

		err = rows.Scan(&temporaryKomisjaObwodowaNadkomisja.IdNadkomisji, &temporaryKomisjaObwodowaNadkomisja.IdKomisji)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryKomisjaObwodowaNadkomisja)
	}
	return result
}

func GetPodkomisje(givenId int) []KomisjaObwodowaNadkomisja {

	var result []KomisjaObwodowaNadkomisja
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM komisje_obwodowe_nadkomisje WHERE id_nadkomisji = givenId")
	
	if err != nil {
		panic(err)
	}

	var temporaryKomisjaObwodowaNadkomisja KomisjaObwodowaNadkomisja

	for rows.Next() {

		err = rows.Scan(&temporaryKomisjaObwodowaNadkomisja.IdNadkomisji, &temporaryKomisjaObwodowaNadkomisja.IdKomisji)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryKomisjaObwodowaNadkomisja)
	}
	return result
}