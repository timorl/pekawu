package pkw

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"strconv"
)

type Nadkomisja struct {
	Id           int
	Nazwa        string
	IdNadkomisji sql.NullInt64
}

func NadkomisjaFromForm(form map[string][]string) (Nadkomisja, error) {
	var result Nadkomisja
	if val, ok := form["nazwa"]; ok {
		result.Nazwa = val[0]
	} else {
		return result, errors.New("Brak nazwy")
	}
	if val, ok := form["id_nadkomisji"]; ok {
		var err error
		result.IdNadkomisji.Int64, err = strconv.ParseInt(val[0], 0, 64)
		if err == nil {
			result.IdNadkomisji.Valid = true
		} else {
			result.IdNadkomisji.Valid = false
		}
	} else {
		result.IdNadkomisji.Valid = false
	}

	return result, nil
}

func AddNadkomisja(n Nadkomisja) bool {

	db, err := OpenDatabase()

	if err != nil {
		return false
	}
	_, err = db.Exec("INSERT INTO nadkomisje (nazwa, id_nadkomisji) VALUES ($1, $2)", n.Nazwa, n.IdNadkomisji)

	if err != nil {
		return false
	}
	return true
}

func GetNadkomisje() []Nadkomisja {

	var result []Nadkomisja
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT id, nazwa, id_nadkomisji FROM nadkomisje")

	if err != nil {
		panic(err)
	}

	var temporaryNadkomisja Nadkomisja

	for rows.Next() {

		err = rows.Scan(&temporaryNadkomisja.Id, &temporaryNadkomisja.Nazwa, &temporaryNadkomisja.IdNadkomisji)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryNadkomisja)
	}
	return result
}

func GetNadkomisjaOfGivenId(givenId int) Nadkomisja {

	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT id, nazwa, id_nadkomisji FROM komisje_obwodowe WHERE id=givenId")

	if err != nil {
		panic(err)
	}

	var temporaryNadkomisja Nadkomisja

	for rows.Next() {
		err = rows.Scan(&temporaryNadkomisja.Id, &temporaryNadkomisja.Nazwa, &temporaryNadkomisja.IdNadkomisji)
		if err != nil {
			panic(err)
		}
	}

	return temporaryNadkomisja
}

func GetNadkomisjaOfGivenWyboryAndObwod(IdWyborow, IdObwodu int) int {
	var rows *sql.Rows
	var err error
	var IdNadkomisji int

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT n.id FROM wybory w JOIN mandaty m ON m.id_wyborow = w.id JOIN nadkomisje n ON n.id = m.id_nadkomisji JOIN komisje_obwodowe_nadkomisje kon ON kon.id_nadkomisji = n.id WHERE w.id=$1 AND kon.id_komisji = $2", IdWyborow, IdObwodu)

	if err != nil {
		panic(err)
	}

	for rows.Next() {

		err = rows.Scan(&IdNadkomisji)
		if err != nil {
			panic(err)
		}
	}

	return IdNadkomisji

}

func GetNadkomisjeOfGivenWybory(IdWyborow int) []int {
	var rows *sql.Rows
	var err error
	var result []int
	var IdNadkomisji int

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT n.id FROM wybory w JOIN mandaty m ON m.id_wyborow = w.id JOIN nadkomisje n ON n.id = m.id_nadkomisji WHERE w.id=$1", IdWyborow)

	if err != nil {
		panic(err)
	}

	for rows.Next() {

		err = rows.Scan(&IdNadkomisji)
		if err != nil {
			panic(err)
		}
		result = append(result, IdNadkomisji)
	}

	return result

}
