package pkw

import (
	"database/sql"
	_ "github.com/lib/pq"
)

type Glosy_Niewazne struct {
	Obwod         int
	IdWyborow     int
	Id_Nadkomisji int
	Liczba        int
}

func VoteIllegal(idKomisji, IdWyborow, Id_Nadkomisji int, Pesel string) bool {
	var err error
	var exists bool
	var rows *sql.Rows

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT EXISTS(SELECT * FROM glosy_niewazne WHERE obwod=$1 AND id_wyborow=$2 AND id_nadkomisji=$3)",
		idKomisji, IdWyborow, Id_Nadkomisji)

	if err != nil {
		panic(err)
	}

	rows.Next()
	err = rows.Scan(&exists)
	if err != nil {
		panic(err)
	}

	if exists {
		_, err = db.Query("UPDATE glosy_niewazne SET liczba = (liczba + 1) WHERE obwod=$1 AND id_wyborow=$2 AND id_nadkomisji=$3",
			idKomisji, IdWyborow, Id_Nadkomisji)
		if err != nil {
			panic(err)
		}
	} else {
		_, err = db.Query("INSERT INTO glosy_niewazne (obwod, id_wyborow, id_nadkomisji, liczba) VALUES ($1, $2, $3, 1)", idKomisji, IdWyborow, Id_Nadkomisji)
		if err != nil {
			panic(err)
		}
	}

	_, err = db.Exec("UPDATE obywatele_obwody SET oddal_glos = TRUE WHERE pesel = $1 AND id_wyborow = $2", Pesel, IdWyborow)

	if err != nil {
		return false
	}

	return true
}
