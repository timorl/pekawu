package pkw

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
)

type KomisjaObwodowa struct {
	Id    int
	Nazwa string
}
type Wynik struct {
	Imie			string
	Nazwisko		string
	NazwaKomitetu	string
	Glosy			int
}

func KomisjaObwodowaFromForm(form map[string][]string) (KomisjaObwodowa, error) {
	var result KomisjaObwodowa
	if val, ok := form["nazwa"]; ok {
		result.Nazwa = val[0]
	} else {
		return result, errors.New("Brak nazwy")
	}
	return result, nil
}

func AddKomisjaObwodowa(k KomisjaObwodowa) (int, error) {
	db, err := OpenDatabase()
	if err != nil {
		return -1, err
	}
	var idKomisji int
	err = db.QueryRow("INSERT INTO komisje_obwodowe (nazwa) VALUES ($1) RETURNING id", k.Nazwa).Scan(&idKomisji)
	return idKomisji, err
}

func AddCzlonekToKomisjaObwodowa(pesel string, idKomisji int, funkcja string) bool {
	db, err := OpenDatabase()

	if err != nil {
		return false
	}
	_, err = db.Exec("INSERT INTO czlonkowie_komisji_obwodowych (pesel, id_komisji, funkcja) VALUES ($1, $2, $3)", pesel, idKomisji, funkcja)

	if err != nil {
		return false
	}
	return true
}

func AddCzlonkowieKomisjiObwodowychFromForm(form map[string][]string, idKomisji int) int {
	var result int
	if val, ok := form["przewodniczacy"]; ok {
		if AddCzlonekToKomisjaObwodowa(val[0], idKomisji, "przewodniczacy") {
			result++
		}
	}
	if val, ok := form["zastepca"]; ok {
		if AddCzlonekToKomisjaObwodowa(val[0], idKomisji, "zastepca") {
			result++
		}
	}
	if val, ok := form["sekretarz"]; ok {
		if AddCzlonekToKomisjaObwodowa(val[0], idKomisji, "sekretarz") {
			result++
		}
	}
	if val, ok := form["czlonek"]; ok {
		for _, psl := range val {
			if psl != "" && AddCzlonekToKomisjaObwodowa(psl, idKomisji, "czlonek") {
				result++
			}
		}
	}
	return result
}

func GetKomisjeObwodowe() []KomisjaObwodowa {

	var result []KomisjaObwodowa
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT id, nazwa FROM komisje_obwodowe")

	if err != nil {
		panic(err)
	}

	var temporaryKomisjaObwodowa KomisjaObwodowa

	for rows.Next() {

		err = rows.Scan(&temporaryKomisjaObwodowa.Id, &temporaryKomisjaObwodowa.Nazwa)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryKomisjaObwodowa)
	}
	return result
}

func GetKomisjaObwodowaOfGivenId(givenId int) KomisjaObwodowa {

	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT id, nazwa FROM komisje_obwodowe WHERE id=givenId")

	if err != nil {
		panic(err)
	}

	var temporaryKomisjaObwodowa KomisjaObwodowa
	
	for rows.Next() {
		err = rows.Scan(&temporaryKomisjaObwodowa.Id, &temporaryKomisjaObwodowa.Nazwa)
		if err != nil {
			panic(err)
		}
	}

	return temporaryKomisjaObwodowa
}

func GetReport(IdKomisji, IdWyborow int) []Wynik {

	var rows *sql.Rows
	var err error
	var result []Wynik
	
	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT o.imie, o.nazwisko, kom.nazwa, coalesce(g.liczba,0)"+ 
							" FROM komisje_obwodowe ko"+ 
							" JOIN komisje_obwodowe_nadkomisje kon ON ko.id = kon.id_komisji"+  
							" JOIN nadkomisje n ON n.id = kon.id_nadkomisji"+  
							" JOIN listy l ON l.id_nadkomisji = n.id"+ 
							" JOIN komitety kom ON l.id_komitetu = kon.id"+ 
							" JOIN kandydaci k ON k.id_listy = l.id"+ 
							" JOIN obywatele o ON o.pesel = k.pesel"+ 
							" LEFT OUTER JOIN glosy g ON g.id_kand=k.id"+   
							" WHERE l.id_wyborow=$1 AND ko.id=$2 AND g.obwod=$2 ORDER BY 3 DESC", IdWyborow, IdKomisji)
	if err != nil {
		panic(err)
	}

	var temporaryWynik Wynik

	for rows.Next() {
		err = rows.Scan(&temporaryWynik.Imie, &temporaryWynik.Nazwisko, &temporaryWynik.NazwaKomitetu, &temporaryWynik.Glosy)
		if err != nil {
			panic(err)
		}
		result = append(result, temporaryWynik)
	}
	
	var liczbaNiewaznych int
	rows, err = db.Query("SELECT  count(*)"+
							" FROM glosy_niewazne"+  
							" WHERE id_wyborow=$1 AND obwod=$2", IdWyborow, IdKomisji)
	if err != nil {
		panic(err)
	}
	
	for rows.Next() {
		err = rows.Scan(&liczbaNiewaznych)
		if err != nil {
			panic(err)
		}
	}
	
	if liczbaNiewaznych>0{
		rows, err = db.Query("SELECT  liczba"+
							" FROM glosy_niewazne"+ 
							" WHERE id_wyborow=$1 AND obwod=$2", IdWyborow, IdKomisji)
		if err != nil {
			panic(err)
		}
	
		for rows.Next() {
			err = rows.Scan(&liczbaNiewaznych)
			if err != nil {
				panic(err)
			}
		}
	}
	
	temporaryWynik.Imie="Glos"
	temporaryWynik.Nazwisko="Niewazny"
	temporaryWynik.NazwaKomitetu="-"
	temporaryWynik.Glosy=liczbaNiewaznych
	result = append(result, temporaryWynik)

	return result
}

func GetFrekwencjaInKomisja(IdKomisji, IdWyborow int) float32 {
	var rows *sql.Rows
	var err error
	var result float32
	var liczbaOddanych int
	var liczbaNiewaznych int
	var liczbaUprawnionych int
	
	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}
	
	rows, err = db.Query("SELECT coalesce(SUM(liczba),0) FROM glosy g JOIN kandydaci k ON g.id_kand=k.id JOIN listy l ON k.id_listy = l.id WHERE l.id_wyborow=$1 AND g.obwod=$2", IdWyborow, IdKomisji)
	if err != nil {
		panic(err)
	}
	
	for rows.Next() {
			err = rows.Scan(&liczbaOddanych)
			if err != nil {
				panic(err)
			}
		}
		
	rows, err = db.Query("SELECT coalesce(SUM(liczba),0) FROM glosy_niewazne WHERE id_wyborow=$1 AND obwod=$2", IdWyborow, IdKomisji)
	if err != nil {
		panic(err)
	}
	
	for rows.Next() {
			err = rows.Scan(&liczbaNiewaznych)
			if err != nil {
				panic(err)
			}
		}
		
	rows, err = db.Query("SELECT coalesce(COUNT(*),0) FROM obywatele_obwody WHERE id_wyborow=$1 AND id_komisji_obwodowej=$2", IdWyborow, IdKomisji)
	if err != nil {
		panic(err)
	}
	
	for rows.Next() {
			err = rows.Scan(&liczbaUprawnionych)
			if err != nil {
				panic(err)
			}
		}
		
	result = (float32(liczbaNiewaznych) + float32(liczbaOddanych))/float32(liczbaUprawnionych)

	return result
}
