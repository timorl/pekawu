package pkw

import (
	"database/sql"
	_ "github.com/lib/pq"
)

type TypWyborow struct {
	Nazwa                string
	LiczbaPodpisow       int
	MinimalnyWiek        int
	SposobLiczeniaGlosow string
}

func AddTypWyborow(t TypWyborow) bool {

	db, err := OpenDatabase()

	if err != nil {
		panic(err)
		return false
	}
	_, err = db.Exec("INSERT INTO typy_wyborow VALUES ($1, $2, $3, $4)", t.Nazwa, t.LiczbaPodpisow, t.MinimalnyWiek, t.SposobLiczeniaGlosow)

	if err != nil {
		panic(err)
		return false
	}
	return true
}

func GetTypyWyborow() []TypWyborow {

	var result []TypWyborow
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM typy_wyborow")

	if err != nil {
		panic(err)
	}

	var temporaryTypWyborow TypWyborow

	for rows.Next() {

		err = rows.Scan(&temporaryTypWyborow.Nazwa, &temporaryTypWyborow.LiczbaPodpisow, &temporaryTypWyborow.MinimalnyWiek, &temporaryTypWyborow.SposobLiczeniaGlosow)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryTypWyborow)
	}
	return result
}
