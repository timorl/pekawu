package pkw

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"time"
)

type Wybory struct {
	Poczatek time.Time
	Koniec   time.Time
	Typ      string
	Id       int
	Nazwa    string
}

func WyboryFromForm(form map[string][]string) (Wybory, error) {
	var result Wybory
	if val, ok := form["poczatek"]; ok {
		var err error
		result.Poczatek, err = time.Parse(time.RFC3339, val[0]+":00Z")
		if err != nil {
			return result, errors.New("Niepoprawna data początku")
		}
	} else {
		return result, errors.New("Niepoprawna data początku")
	}
	if val, ok := form["koniec"]; ok {
		var err error
		result.Koniec, err = time.Parse(time.RFC3339, val[0]+":00Z")
		if err != nil {
			return result, errors.New("Niepoprawna data końca")
		}
	} else {
		return result, errors.New("Niepoprawna data końca")
	}
	if val, ok := form["typ"]; ok {
		result.Typ = val[0]
	} else {
		return result, errors.New("Brak typu")
	}
	if val, ok := form["nazwa"]; ok {
		result.Nazwa = val[0]
	} else {
		return result, errors.New("Brak nazwy")
	}
	return result, nil
}

func AddWybory(w Wybory) bool {
	db, err := OpenDatabase()

	if err != nil {
		return false
	}
	_, err = db.Exec("INSERT INTO wybory (poczatek, koniec, typ, nazwa) VALUES ($1, $2, $3, $4)", w.Poczatek, w.Koniec, w.Typ, w.Nazwa)

	if err != nil {
		return false
	}
	return true
}

func GetWybory() []Wybory {
	var result []Wybory
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT poczatek, koniec, typ, id, nazwa FROM wybory")

	if err != nil {
		panic(err)
	}

	var temporaryWybory Wybory

	for rows.Next() {

		err = rows.Scan(&temporaryWybory.Poczatek, &temporaryWybory.Koniec, &temporaryWybory.Typ, &temporaryWybory.Id, &temporaryWybory.Nazwa)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryWybory)
	}
	return result
}

func GetPastWybory() []Wybory {
	var result []Wybory
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT poczatek, koniec, typ, id, nazwa FROM wybory WHERE now()>koniec")

	if err != nil {
		panic(err)
	}

	var temporaryWybory Wybory

	for rows.Next() {

		err = rows.Scan(&temporaryWybory.Poczatek, &temporaryWybory.Koniec, &temporaryWybory.Typ, &temporaryWybory.Id, &temporaryWybory.Nazwa)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryWybory)
	}
	return result
}

func GetCurrentWyboryOfAGivenObywatel(givenPesel string) []Wybory {
	var result []Wybory
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT w.poczatek, w.koniec, w.typ, w.id, w.nazwa FROM wybory w JOIN obywatele_obwody ow ON w.id=ow.id_wyborow JOIN obywatele o ON o.pesel = ow.pesel WHERE o.pesel = $1 AND now()<=w.koniec AND now()>=w.poczatek AND NOT ow.oddal_glos", givenPesel)

	if err != nil {
		panic(err)
	}

	var temporaryWybory Wybory

	for rows.Next() {

		err = rows.Scan(&temporaryWybory.Poczatek, &temporaryWybory.Koniec, &temporaryWybory.Typ, &temporaryWybory.Id, &temporaryWybory.Nazwa)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryWybory)
	}
	return result
}

func GetResultOfGivenObywatel(IdWyborow int, Pesel string) int {
	var result int
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT coalesce(SUM(liczba),0) FROM glosy g JOIN kandydaci k ON g.id_kand=k.id JOIN listy l ON k.id_listy = l.id WHERE k.pesel =$1 AND l.id_wyborow=$2", Pesel, IdWyborow)

	if err != nil {
		panic(err)
	}

	for rows.Next() {

		err = rows.Scan(&result)
		if err != nil {
			panic(err)
		}
	}
	return result

}

func GetFrekwencjaInWybory(IdWyborow int) float32 {
	var rows *sql.Rows
	var err error
	var result float32
	var liczbaOddanych int
	var liczbaNiewaznych int
	var liczbaUprawnionych int

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT coalesce(SUM(liczba),0) FROM glosy g JOIN kandydaci k ON g.id_kand=k.id JOIN listy l ON k.id_listy = l.id WHERE l.id_wyborow=$1", IdWyborow)
	if err != nil {
		panic(err)
	}

	for rows.Next() {
		err = rows.Scan(&liczbaOddanych)
		if err != nil {
			panic(err)
		}
	}

	rows, err = db.Query("SELECT coalesce(SUM(liczba),0) FROM glosy_niewazne WHERE id_wyborow=$1", IdWyborow)
	if err != nil {
		panic(err)
	}

	for rows.Next() {
		err = rows.Scan(&liczbaNiewaznych)
		if err != nil {
			panic(err)
		}
	}

	rows, err = db.Query("SELECT coalesce(COUNT(*),0) FROM obywatele_obwody WHERE id_wyborow=$1", IdWyborow)
	if err != nil {
		panic(err)
	}

	for rows.Next() {
		err = rows.Scan(&liczbaUprawnionych)
		if err != nil {
			panic(err)
		}
	}

	result = (float32(liczbaNiewaznych) + float32(liczbaOddanych)) / float32(liczbaUprawnionych)

	return result
}
