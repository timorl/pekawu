package pkw

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"strconv"
	"time"
)

type Obywatel struct {
	Pesel           string
	Data_urodzenia  time.Time
	Nr_dowodu       sql.NullString
	Imie            string
	Nazwisko        string
	Miasto          string
	Kod_pocztowy    string
	Ulica           string
	Nr_domu         string
	Literka_domu    sql.NullString
	Nr_mieszkania   sql.NullInt64
	Pozbawiony_praw bool
}

func ObywatelFromForm(form map[string][]string) (Obywatel, error) {
	var result Obywatel
	if val, ok := form["pesel"]; ok {
		result.Pesel = val[0]
	} else {
		return result, errors.New("Brak PESELu")
	}
	if val, ok := form["data_urodzenia"]; ok {
		var err error
		result.Data_urodzenia, err = time.Parse(time.RFC3339, val[0]+"T00:00:00Z")
		if err != nil {
			panic(err)
		}
	} else {
	}
	if val, ok := form["nr_dowodu"]; ok {
		result.Nr_dowodu.String = val[0]
		result.Nr_dowodu.Valid = true
	} else {
		result.Nr_dowodu.Valid = false
	}
	if val, ok := form["imie"]; ok {
		result.Imie = val[0]
	} else {
		return result, errors.New("Brak imienia")
	}
	if val, ok := form["nazwisko"]; ok {
		result.Nazwisko = val[0]
	} else {
		return result, errors.New("Brak nazwiska")
	}
	if val, ok := form["miasto"]; ok {
		result.Miasto = val[0]
	} else {
		return result, errors.New("Błędny adres")
	}
	if val, ok := form["kod_pocztowy"]; ok {
		result.Kod_pocztowy = val[0]
	} else {
		return result, errors.New("Błędny adres")
	}
	if val, ok := form["ulica"]; ok {
		result.Ulica = val[0]
	} else {
		return result, errors.New("Błędny adres")
	}
	if val, ok := form["nr_domu"]; ok {
		result.Nr_domu = val[0]
	} else {
		return result, errors.New("Błędny adres")
	}
	if val, ok := form["literka_domu"]; ok {
		result.Literka_domu.String = val[0]
		result.Literka_domu.Valid = true
	} else {
		result.Literka_domu.Valid = false
	}
	if val, ok := form["nr_mieszkania"]; ok {
		var err error
		result.Nr_mieszkania.Int64, err = strconv.ParseInt(val[0], 0, 64)
		if err != nil {
			result.Nr_mieszkania.Valid = true
		} else {
			result.Nr_mieszkania.Valid = false
		}
	} else {
		result.Nr_mieszkania.Valid = false
	}
	result.Pozbawiony_praw = false
	return result, nil
}

func AddObywatel(o Obywatel) bool {

	db, err := OpenDatabase()

	if err != nil {
		panic(err)
		return false
	}
	_, err = db.Exec("INSERT INTO obywatele VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)",
		o.Pesel, o.Data_urodzenia, o.Nr_dowodu, o.Imie, o.Nazwisko, o.Miasto, o.Kod_pocztowy,
		o.Ulica, o.Nr_domu, o.Literka_domu, o.Nr_mieszkania, o.Pozbawiony_praw)

	if err != nil {
		panic(err)
		return false
	}
	return true
}

func SearchObywatele(preSearchTerms map[string][]string) []Obywatel {

	var result []Obywatel
	var rows *sql.Rows
	var err error
	var searchTerms map[string][]string
	searchTerms = make(map[string][]string)
	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}
	
	for key, value := range preSearchTerms {
        if value[0] != ""{
			searchTerms[key]=value
		}
    }

	if val, ok := searchTerms["peselLubNrDowodu"]; ok {
		rows, err = db.Query("SELECT * FROM obywatele WHERE pesel = $1 OR nr_dowodu = $1", val[0])
	} else if val1, ok1 := searchTerms["idKomisjiObwodowej"]; ok1 {

		if val2, ok2 := searchTerms["idWyborow"]; ok2 {
			rows, err = db.Query("SELECT o.* FROM obywatele o JOIN obywatele_obwody oo WHERE o.pesel = oo.pesel AND oo.id_komisji_obwodowej = $1 AND oo.id_wyborow = $2", val1[0], val2[0])
		}

	} else if val1, ok1 := searchTerms["nr_parzyste"]; ok1 {
		if val2, ok2 := searchTerms["ulica"]; ok2 {
			if val3, ok3 := searchTerms["kodPocztowy"]; ok3 {
				if val4, ok4 := searchTerms["miasto"]; ok4 {
					rows, err = db.Query("SELECT * FROM obywatele"+
						" WHERE nr_domu%2 = $1"+
						" AND ulica = $2"+
						" AND kod_pocztowy = $3"+
						" AND miasto = $4", val1[0], val2[0], val3[0], val4[0])
				} else {
					rows, err = db.Query("SELECT * FROM obywatele"+
						" WHERE nr_domu%2 = $1"+
						" AND ulica = $2"+
						" AND kod_pocztowy = $3", val1[0], val2[0], val3[0])
				}
			} else if val3, ok3 := searchTerms["miasto"]; ok3 {
				rows, err = db.Query("SELECT * FROM obywatele"+
					" WHERE nr_domu%2 = $1"+
					" AND ulica = $2"+
					" AND miasto = $3", val1[0], val2[0], val3[0])
			}
		}

	} else if val1, ok1 := searchTerms["ulica"]; ok1 {
		if val2, ok2 := searchTerms["kodPocztowy"]; ok2 {
			if val3, ok3 := searchTerms["miasto"]; ok3 {
				rows, err = db.Query("SELECT * FROM obywatele"+
					" WHERE ulica = $1"+
					" AND kod_pocztowy = $2"+
					" AND miasto = $3", val1[0], val2[0], val3[0])
			} else {
				rows, err = db.Query("SELECT * FROM obywatele"+
					" WHERE ulica = $1"+
					" AND kod_pocztowy = $2", val1[0], val2[0])
			}
		} else if val2, ok2 := searchTerms["miasto"]; ok2 {
			rows, err = db.Query("SELECT * FROM obywatele"+
				" WHERE ulica = $1"+
				" AND miasto = $2", val1[0], val2[0])
		}
	} else if val1, ok1 := searchTerms["kodPocztowy"]; ok1 {

		if val2, ok2 := searchTerms["miasto"]; ok2 {
			rows, err = db.Query("SELECT * FROM obywatele WHERE kod_pocztowy = $1 AND miasto = $2", val1[0], val2[0])
		} else {
			rows, err = db.Query("SELECT * FROM obywatele WHERE kod_pocztowy = $1", val1[0])
		}
	} else if val1, ok1 := searchTerms["miasto"]; ok1 {
		rows, err = db.Query("SELECT * FROM obywatele WHERE miasto = $1", val1[0])
	}
	if err != nil {
		panic(err)
	}

	var temporaryObywatel Obywatel

	for rows.Next() {

		err = rows.Scan(&temporaryObywatel.Pesel, &temporaryObywatel.Data_urodzenia,
			&temporaryObywatel.Nr_dowodu, &temporaryObywatel.Imie,
			&temporaryObywatel.Nazwisko, &temporaryObywatel.Miasto,
			&temporaryObywatel.Kod_pocztowy, &temporaryObywatel.Ulica,
			&temporaryObywatel.Nr_domu, &temporaryObywatel.Literka_domu,
			&temporaryObywatel.Nr_mieszkania, &temporaryObywatel.Pozbawiony_praw)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryObywatel)
	}
	return result
}
