package pkw

import (
	"database/sql"
	_ "github.com/lib/pq"
)

type Glosy struct {
	Obwod  int
	IdKand int
	Liczba int
}

func AddGlosy(g Glosy) bool {

	db, err := OpenDatabase()

	if err != nil {
		return false
	}
	_, err = db.Exec("INSERT INTO glosy VALUES ($1, $2, $3)", g.Obwod, g.IdKand, g.Liczba)

	if err != nil {
		return false
	}
	return true
}

func GetGlosy() []Glosy {

	var result []Glosy
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM glosy")

	if err != nil {
		panic(err)
	}

	var temporaryGlosy Glosy

	for rows.Next() {

		err = rows.Scan(&temporaryGlosy.Obwod, &temporaryGlosy.IdKand, &temporaryGlosy.Liczba)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryGlosy)
	}
	return result
}

func Vote(idKomisji, idKandydata int, Pesel string) bool {

	var err error
	var exists bool
	var rows *sql.Rows

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT EXISTS(SELECT * FROM glosy WHERE obwod=$1 AND id_kand=$2)", idKomisji, idKandydata)

	if err != nil {
		panic(err)
	}

	rows.Next()
	err = rows.Scan(&exists)
	if err != nil {
		panic(err)
	}

	if exists {
		_, err = db.Query("UPDATE glosy SET liczba = (liczba + 1) WHERE obwod=$1 AND id_kand=$2", idKomisji, idKandydata)
		if err != nil {
			panic(err)
		}
	} else {
		_, err = db.Query("INSERT INTO glosy (obwod, id_kand, liczba) VALUES ($1, $2, 1)", idKomisji, idKandydata)
		if err != nil {
			panic(err)
		}
	}

	if err != nil {
		panic(err)
	}

	_, err = db.Exec("UPDATE obywatele_obwody SET oddal_glos = TRUE WHERE pesel = $1 AND id_wyborow = (SELECT l.id_wyborow FROM listy l JOIN kandydaci k ON k.id_listy = l.id WHERE k.id=$2)", Pesel, idKandydata)

	if err != nil {
		return false
	}

	return true
}
