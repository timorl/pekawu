package pkw

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"strconv"
)

type Lista struct {
	Id           int
	IdKomitetu   int
	IdNadkomisji int
	IdWyborow    int
}

type ListaZKomitetem struct {
	NazwaKomitetu string
	IdListy       int
}

func ListaFromForm(form map[string][]string) (Lista, error) {
	var result Lista
	if val, ok := form["id_nadkomisji"]; ok {
		idNk, err := strconv.ParseInt(val[0], 0, 0)
		if err != nil {
			return result, errors.New("Brak id nadkomisji")
		}
		result.IdNadkomisji = int(idNk)
	} else {
		return result, errors.New("Brak id nadkomisji")
	}
	if val, ok := form["id_komitetu"]; ok {
		idNk, err := strconv.ParseInt(val[0], 0, 0)
		if err != nil {
			return result, errors.New("Brak id komitetu")
		}
		result.IdKomitetu = int(idNk)
	} else {
		return result, errors.New("Brak id komitetu")
	}
	if val, ok := form["id_wyborow"]; ok {
		idNk, err := strconv.ParseInt(val[0], 0, 0)
		if err != nil {
			return result, errors.New("Brak id wyborow")
		}
		result.IdWyborow = int(idNk)
	} else {
		return result, errors.New("Brak id wyborow")
	}
	return result, nil
}

func AddListy(l Lista) (int, error) {
	db, err := OpenDatabase()

	if err != nil {
		return -1, err
	}
	var idListy int
	err = db.QueryRow("INSERT INTO listy (id_komitetu, id_nadkomisji, id_wyborow) VALUES ($1, $2, $3) RETURNING id", l.IdKomitetu, l.IdNadkomisji, l.IdWyborow).Scan(&idListy)
	return idListy, err
}

func GetListy() []Lista {
	var result []Lista
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT id, id_komitetu, id_nadkomisji, id_wyborow FROM listy")

	if err != nil {
		panic(err)
	}

	var temporaryLista Lista

	for rows.Next() {

		err = rows.Scan(&temporaryLista.Id, &temporaryLista.IdKomitetu, &temporaryLista.IdNadkomisji, &temporaryLista.IdWyborow)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryLista)
	}
	return result
}

func GetListyOfGivenWyboryAndObywatel(givenPesel string, givenIdWyborow int) []Lista {
	var result []Lista
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT l.id, l.id_komitetu, l.id_nadkomisji, l.id_wyborow FROM zatwierdzone_listy zl"+
		" JOIN listy l ON l.id = zl.id"+
		" JOIN nadkomisje n ON n.id = l.id_nadkomisji"+
		" JOIN mandaty m ON m.id_nadkomisji = n.id"+
		" JOIN komisje_obwodowe_nadkomisje kon ON n.id = kon.id_nadkomisji"+
		" JOIN komisje_obwodowe ko ON ko.id = kon.id_komisji"+
		" JOIN obywatele_obwody oo ON oo.id_komisji_obwodowej = ko.id"+
		" JOIN obywatele o ON oo.pesel = o.pesel"+
		" JOIN komitety kom ON kom.id = l.id_komitetu"+
		" WHERE o.pesel = $1 AND m.id_wyborow = $2 AND oo.id_wyborow = $2 ORDER BY kom.nr NULLS LAST", givenPesel, givenIdWyborow)

	if err != nil {
		panic(err)
	}

	var temporaryLista Lista

	for rows.Next() {

		err = rows.Scan(&temporaryLista.Id, &temporaryLista.IdKomitetu, &temporaryLista.IdNadkomisji, &temporaryLista.IdWyborow)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryLista)
	}
	return result
}

func GetKomitetOfLista(listy []Lista) []ListaZKomitetem {
	var result []ListaZKomitetem
	var rows *sql.Rows
	var err error
	var temporaryListaZKomitetem ListaZKomitetem

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	for i := 0; i < len(listy); i++ {

		rows, err = db.Query("SELECT nazwa FROM komitety WHERE id = $1", listy[i].IdKomitetu)
		for rows.Next() {

			err = rows.Scan(&temporaryListaZKomitetem.NazwaKomitetu)
			if err != nil {
				panic(err)
			}
			temporaryListaZKomitetem.IdListy = listy[i].Id

			result = append(result, temporaryListaZKomitetem)
		}
	}

	return result
}
