package pkw

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"strconv"
)

type ObywateleObwody struct {
	Pesel              string
	IdKomisjiObwodowej int
	IdWyborow          int
}

func ObywateleObwodyFromForm(form map[string][]string) ([]ObywateleObwody, error) {
	var result []ObywateleObwody
	var idKomisjiObwodowej int
	var idWyborow int
	if val, ok := form["id_komisji"]; ok {
		id, err := strconv.ParseInt(val[0], 0, 0)
		if err != nil {
			return result, errors.New("Brak komisji")
		} else {
			idKomisjiObwodowej = int(id)
		}
	} else {
		return result, errors.New("Brak komisji")
	}
	if val, ok := form["id_wyborow"]; ok {
		id, err := strconv.ParseInt(val[0], 0, 0)
		if err != nil {
			return result, errors.New("Brak wyborow")
		} else {
			idWyborow = int(id)
		}
	} else {
		return result, errors.New("Brak wyborow")
	}
	if val, ok := form["pesele"]; ok {
		for _, psl := range val {
			var rslt ObywateleObwody
			rslt.Pesel = psl
			rslt.IdKomisjiObwodowej = idKomisjiObwodowej
			rslt.IdWyborow = idWyborow
			result = append(result, rslt)
		}
	} else {
		return result, errors.New("Brak PESELów")
	}
	return result, nil
}

func AddObywateleObwody(oo ObywateleObwody) bool {

	db, err := OpenDatabase()

	if err != nil {
		panic(err)
		return false
	}
	_, err = db.Exec("INSERT INTO obywatele_obwody (pesel, id_komisji_obwodowej, id_wyborow, oddal_glos) VALUES ($1, $2, $3, FALSE)", oo.Pesel, oo.IdKomisjiObwodowej, oo.IdWyborow)

	if err != nil {
		panic(err)
		return false
	}
	return true
}

func GetObywateleObwody() []ObywateleObwody {

	var result []ObywateleObwody
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM obywatele_obwody")

	if err != nil {
		panic(err)
	}

	var temporaryObywateleObwody ObywateleObwody

	for rows.Next() {

		err = rows.Scan(&temporaryObywateleObwody.Pesel, &temporaryObywateleObwody.IdKomisjiObwodowej, &temporaryObywateleObwody.IdWyborow)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryObywateleObwody)
	}
	return result
}

func GetWhereDoesObywatelVote(pesel string, id int) int {

	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT id_komisji_obwodowej FROM obywatele_obwody WHERE pesel = $1 AND id_wyborow = $2", pesel, id)

	if err != nil {
		panic(err)
	}

	var id_komisji int

	rows.Next()
	err = rows.Scan(&id_komisji)
	if err != nil {
		panic(err)
	}

	return id_komisji
}
