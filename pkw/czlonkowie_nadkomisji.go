package pkw

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"strconv"
)

type CzlonekNadkomisji struct {
	Pesel        string
	IdNadkomisji int
	Funkcja      string
}

func CzlonekNadkomisjiFromForm(form map[string][]string) (CzlonekNadkomisji, error) {
	var result CzlonekNadkomisji
	if val, ok := form["pesel"]; ok {
		result.Pesel = val[0]
	} else {
		return result, errors.New("Brak PESELu")
	}
	if val, ok := form["funkcja"]; ok {
		result.Funkcja = val[0]
	} else {
		return result, errors.New("Brak funkcji")
	}
	if val, ok := form["id_nadkomisji"]; ok {
		idNk, err := strconv.ParseInt(val[0], 0, 0)
		if err != nil {
			return result, errors.New("Brak id nadkomisji")
		}
		result.IdNadkomisji = int(idNk)
	} else {
		return result, errors.New("Brak id nadkomisji")
	}
	return result, nil
}

func AddCzlonekNadkomisji(cn CzlonekNadkomisji) bool {

	db, err := OpenDatabase()

	if err != nil {
		panic(err)
		return false
	}
	_, err = db.Exec("INSERT INTO czlonkowie_nadkomisji (pesel, id_nadkomisji, funkcja) VALUES ($1, $2, $3)", cn.Pesel, cn.IdNadkomisji, cn.Funkcja)

	if err != nil {
		panic(err)
		return false
	}
	return true
}

func GetCzlonkowNadkomisji() []CzlonekNadkomisji {

	var result []CzlonekNadkomisji
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM czlonkowie_nadkomisji")

	if err != nil {
		panic(err)
	}

	var temporaryCzlonekNadkomisji CzlonekNadkomisji

	for rows.Next() {

		err = rows.Scan(&temporaryCzlonekNadkomisji.Pesel, &temporaryCzlonekNadkomisji.IdNadkomisji, &temporaryCzlonekNadkomisji.Funkcja)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryCzlonekNadkomisji)
	}
	return result
}

func GetCzlonkowieNadkomisjiOfGivenNadkomisja(givenId int) []CzlonekNadkomisji {

	var result []CzlonekNadkomisji
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM czlonkowie_nadkomisji WHERE id_nadkomisji = givenId")

	if err != nil {
		panic(err)
	}

	var temporaryCzlonekNadkomisji CzlonekNadkomisji

	for rows.Next() {

		err = rows.Scan(&temporaryCzlonekNadkomisji.Pesel, &temporaryCzlonekNadkomisji.IdNadkomisji, &temporaryCzlonekNadkomisji.Funkcja)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryCzlonekNadkomisji)
	}
	return result
}
