package pkw

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"strconv"
)

type Podpis struct {
	Pesel   string
	IdListy int
}

func PodpisyFromForm(form map[string][]string) ([]Podpis, error) {
	var result []Podpis
	var single Podpis
	if val, ok := form["id_listy"]; ok {
		idLst, err := strconv.ParseInt(val[0], 0, 0)
		if err != nil {
			return result, errors.New("Brak id listy")
		}
		single.IdListy = int(idLst)
	} else {
		return result, errors.New("Brak id listy")
	}
	for _, psl := range form["pesel"] {
		if psl != "" {
			single.Pesel = psl
			result = append(result, single)
		}
	}
	return result, nil
}

func AddPodpisy(p Podpis) bool {

	db, err := OpenDatabase()

	if err != nil {
		panic(err)
		return false
	}
	_, err = db.Exec("INSERT INTO podpisy VALUES ($1, $2)", p.Pesel, p.IdListy)

	if err != nil {
		panic(err)
		return false
	}
	return true
}

func GetPodpisy() []Podpis {

	var result []Podpis
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM podpisy")

	if err != nil {
		panic(err)
	}

	var temporaryPodpis Podpis

	for rows.Next() {

		err = rows.Scan(&temporaryPodpis.Pesel, &temporaryPodpis.IdListy)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryPodpis)
	}
	return result
}
