package pkw

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"strconv"
)

type Komitet struct {
	Id    int
	Nazwa string
	Nr    sql.NullInt64
}

func KomitetFromForm(form map[string][]string) (Komitet, error) {
	var result Komitet
	if val, ok := form["nazwa"]; ok {
		result.Nazwa = val[0]
	} else {
		return result, errors.New("Brak nazwy")
	}
	if val, ok := form["nr"]; ok {
		var err error
		result.Nr.Int64, err = strconv.ParseInt(val[0], 0, 64)
		if err != nil {
			result.Nr.Valid = false
		} else {
			result.Nr.Valid = true
		}
	} else {
		result.Nr.Valid = false
	}
	return result, nil
}

func AddKomitety(k Komitet) bool {

	db, err := OpenDatabase()

	if err != nil {
		panic(err)
		return false
	}
	_, err = db.Exec("INSERT INTO komitety (nazwa, nr) VALUES ($1, $2)", k.Nazwa, k.Nr)

	if err != nil {
		panic(err)
		return false
	}
	return true
}

func GetKomitety() []Komitet {

	var result []Komitet
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM komitety")

	if err != nil {
		panic(err)
	}

	var temporaryKomitet Komitet

	for rows.Next() {

		err = rows.Scan(&temporaryKomitet.Id, &temporaryKomitet.Nazwa, &temporaryKomitet.Nr)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryKomitet)
	}
	return result
}

func GetKomitetOfGivenWybory(givenId int) Komitet {

	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM komitety WHERE id=givenId")

	if err != nil {
		panic(err)
	}

	var temporaryKomitet Komitet

	err = rows.Scan(&temporaryKomitet.Id, &temporaryKomitet.Nazwa, &temporaryKomitet.Nr)

	return temporaryKomitet
}
