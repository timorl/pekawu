package pkw

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"strconv"
)

type Kandydat struct {
	Id      int
	IdListy int
	Pesel   string
}

func KandydaciFromForm(form map[string][]string) ([]Kandydat, error) {
	var result []Kandydat
	var single Kandydat
	if val, ok := form["id_listy"]; ok {
		idLst, err := strconv.ParseInt(val[0], 0, 0)
		if err != nil {
			return result, errors.New("Brak id listy")
		}
		single.IdListy = int(idLst)
	} else {
		return result, errors.New("Brak id listy")
	}
	for _, psl := range form["pesel"] {
		if psl != "" {
			single.Pesel = psl
			result = append(result, single)
		}
	}
	return result, nil
}

func AddKandydata(k Kandydat) bool {

	db, err := OpenDatabase()

	if err != nil {
		panic(err)
		return false
	}
	_, err = db.Exec("INSERT INTO kandydaci (id_listy, pesel) VALUES ($1, $2)", k.IdListy, k.Pesel)

	if err != nil {
		panic(err)
		return false
	}
	return true
}

func GetKandydat() []Kandydat {

	var result []Kandydat
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM kandydaci")

	if err != nil {
		panic(err)
	}

	var temporaryKandydat Kandydat

	for rows.Next() {

		err = rows.Scan(&temporaryKandydat.Id, &temporaryKandydat.IdListy, &temporaryKandydat.Pesel)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryKandydat)
	}
	return result
}

func GetKandydaciFromLista(GivenIdListy int) []Kandydat {

	var result []Kandydat
	var rows *sql.Rows
	var err error

	db, err := OpenDatabase()
	if err != nil {
		panic(err)
	}

	rows, err = db.Query("SELECT * FROM kandydaci WHERE id_listy = $1", GivenIdListy)

	if err != nil {
		panic(err)
	}

	var temporaryKandydat Kandydat

	for rows.Next() {

		err = rows.Scan(&temporaryKandydat.Id, &temporaryKandydat.IdListy, &temporaryKandydat.Pesel)
		if err != nil {
			panic(err)
		}

		result = append(result, temporaryKandydat)
	}
	return result
}
